<?php
/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

!defined("IN_APP") && exit();

#header('P3P: CP="CAO PSA OUR"');
header("Content-Type:text/html;charset=utf-8");
unset($GLOBALS);

/**
 * 开启输出缓冲
 */
require "lib/func.ob.php";
//ob_start("ob_gzip");
ob_start();

/**
 * 全局约定配置
 */

$C = require "#@.php";

require "lib/func.utils.php";
require "lib/func.common.php";

/**
 * 模板配置开始 初始化模板引擎
 */
import("vendor/smarty/Smarty.class");
$tpl = new Smarty();
foreach($C['tpl'] as $k=> $cnf) {
        $tpl->$k = $cnf;
}
unset($k);
unset($cnf);

/**
 * 载入smarty插件
 */
load_smarty_tag();

/**
 * 初始化插件
 */
import("lib/class.pluggable");
Pluggable::init();

/**
 * 注册结束时的函数
 */
import("lib/func.when_shutdown");

/**
 * 泛解析二级域名
 */
if(!isset($C['base']['runmode']) or $C['base']['runmode'] != 'devel') {
    $sd = array_shift(explode(".", $_SERVER["HTTP_HOST"]));
    if(!in_array($sd, $C["denied_domain_list"]) and $_SERVER["HTTP_HOST"] !== $C["base"]["host"]) {
        $_SERVER["REQUEST_URI"] = $_SERVER["REQUEST_URI"] == "/" ? "" : $_SERVER["REQUEST_URI"];
        $_SERVER["REQUEST_URI"] = sprintf("/view/%s%s", $sd, $_SERVER["REQUEST_URI"]);
    }
    unset($sd);
    unset($uri);
}


/**
 * URI解析/分发
 */
import("lib/class.dispatcher");
Dispatcher::$type = $C["base"]["url_type"];
$action = Dispatcher::work();

$action["group"] = $action["group"] ? $action["group"] : "home";
$action["name"] = $action["name"] ? $action["name"] : "index";
$action_file = PROJECT_DIR.DS."actions".DS.$action["group"].DS.$action["name"].".php";

if(!is_file($action_file)) {
    $action["not_found"] = $action["name"];
    $action["source_name"] = $action["name"];
    $action["name"] = "index";
    $action_file = PROJECT_DIR.DS."actions".DS.$action["group"].DS.$action["name"].".php";
    if(!is_file($action_file)) {
        /**
         * 首先判断是否存在模板文件， 存在则直接显示
         */
        if($action["params"]) {
            $the_param = "/".implode("/", $action["params"]);
        }
        $is_tpl = sprintf("%s/%s%s.tpl", $action["group"], $action["source_name"], $the_param);
        if($tpl->templateExists($is_tpl)) {
            $include_tpl = true;
        } else {
            r("error/404");
        }
    }
}

$C["action"] = $action;


/**
 * 引入常用包
 */
import("lib/database/pdo");
import("lib/class.model");
import("actions/account/class.user");
import("actions/blog/func.blog");
import("actions/account/func.account");

/**
 * session
 */

import("lib/class.session");
/**
 * swf upload
 */
if($_GET["UPLOAD_HASH"]) {
    session_id($_GET["UPLOAD_HASH"]);
}
session_start();

/**
 * 实例化用户信息
 */
$user = new User();
$tpl->assign("user", $user);

$tpl->assign("C", $C);

if($include_tpl) {
    $tpl->display($is_tpl);
    exit; 
} else {
    require $action_file;
}