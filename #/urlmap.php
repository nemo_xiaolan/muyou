<?php

/**
 * URL Map 定义
 * map => 映射到真实url
 * 要实现的效果：
 *  blog/muyou/publish/text  => /actions/publish/text.php?blog=muyou
 */

$__allow_blog_domain = "[a-z0-9\-]{5,15}";

return array(
    /**
     * 单独某个博客博客发表页面
     */
    "blog\/(?P<blog_name>{$__allow_blog_domain})\/publish\/(?P<action>\w+)" => "publish/:action?blog=:blog_name",
    
    /**
     * 我喜欢的文章
     */
    "favorites" => "posts/favorites",
    
    /**
     * 博客详情页面 
     * @todo 多用户二级域名支持 rewrite实现
     */
    "view\/(?P<blog_name>{$__allow_blog_domain})$" => "blog/detail?blog=:blog_name",
    
    /**
     * 博客文章详情
     * @todo 文章别名
     */
    "view\/(?P<blog_name>{$__allow_blog_domain})\/posts\/(?P<post_id>\d+)" => "blog/detail/posts/:post_id?blog=:blog_name",
    
    /**
     * 图片处理
     */
    "thumbnail\/(?P<dir>[a-zA-Z0-9\-_]+)\/(?P<filename>[a-z0-9]+)_(?P<width>\d+)_(?P<height>\d+)\.(?P<ext>(jpg|jpeg|png|gif))"
        => "posts/rewrite_thumbnail?dir=:dir&filename=:filename&w=:width&h=:height",
    
    /**
     * 博客设置
     */
    "customize\/(?P<blog_name>{$__allow_blog_domain})" => "blog/customize/:blog_name",
            
    /**
     * 账户设置
     */
    "settings" => "account/settings",
            
    /**
     * 关注的博客设置
     */
    "following" => "blog/following",
     
    /**
     * 探索
     */
    "explore" => "category/explore",
);
