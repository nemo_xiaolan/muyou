<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 * 
 * 搜索所有标签
 */

$keyword = urldecode($action["params"][0]);

import("actions/tags/class.tags");
$tags = new Tags();
$the_itemids = $tags->get_items_by_tag($keyword);

if($the_itemids) {
    import("actions/posts/class.posts");
    $post = new Posts();
    $the_posts = $post->get_by_ids($the_itemids, $user->uid);
    
    $tpl->assign("paginator", $the_posts);
}

$tpl->display("search/result.tpl");