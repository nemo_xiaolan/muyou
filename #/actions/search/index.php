<?php

/**
 * 搜索功能总入口
 */
$allow = array(
    "all_tags", "my_fav", "my_posts", "i_like"
);

$target = $_POST["search_target"] ? $_POST["search_target"] : "all_tags";


if(!in_array($target, $allow)) {
    r("home");
}

$keyword = mysql_escape_string($_POST["search_keyword"]);
$keyword = urlencode($keyword);

r(sprintf("search/%s/%s", $target, $keyword));
