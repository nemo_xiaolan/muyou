<?php

/**
 * 需要登陆
 * 如果未登陆 则跳转到到登陆口
 */
function need_login() {
    global $user;
    global $C;
    $subdir = str_ireplace("index.php", "", $_SERVER["PHP_SELF"]);
    $next = end(explode($subdir, $_SERVER["REQUEST_URI"]));
    if(!$user->is_login) {
        r("account/login?next=".urlencode($next));
    }
}
