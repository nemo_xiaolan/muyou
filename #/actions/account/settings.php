<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

need_login();

if(is_post()) {
    $current_pwd = trim($_POST["current_password"]);
    $new_pwd = trim($_POST["new_password"]);
    $new_pwd_repeat = trim($_POST["new_password_repeat"]);
    $errors = "";
    /**
     * 修改密码
     */
    if($current_pwd and $new_pwd and $new_pwd_repeat) {
        $the_user = $user->get_by($user->uid);
        list(, $start, $length) = explode('__', $the_user['password']);
        if($current_pwd == $new_pwd) {
            $error = "新密码和旧密码一样";
        } else {
            if($the_user["password"] != $user->generate_password($current_pwd, $start, $length)) {
                $error = "当前密码不正确";
            } else {
                if($new_pwd_repeat !== $new_pwd) {
                    $error = "新密码和重复密码不相同";
                } else {
                    $user->edit($user->uid, array(
                        "password" => $user->generate_password($new_pwd)
                    ));
                }
            }
        }
    }
    
    if(!$error) {
        r("home");
    }
    
    $tpl->assign("error", $error);
}

$tpl->display("account/settings.tpl");