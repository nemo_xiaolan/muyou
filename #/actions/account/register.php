<?php

if($user->is_login) {
    r("home");
}

if(is_post()) {
    $email = trim($_POST["email"]);
    $pwd  = trim($_POST["password"]);
    $sub_domain = trim($_POST["sub_domain"]);
    
    if(!is_email($email)) {
        $errors['email'] = "邮箱格式不对";
    }
    
    if(strlen($pwd) < 6) {
        $errors["pwd"] = "密码不能小于6位";
    }
    
    if(strlen($sub_domain) < 5) {
        $errors["sub_domain"] = "个性域名不能小于6位";
    }
    
    if(!preg_match('/[a-z0-9\-]{5,15}/', $sub_domain)) {
        $errors["sub_domain"] = "只用使用5-15位小写字母,数字和 -";
    }
    
    if(!$errors) {
        $the_user = $user->register($email, $pwd);
        if(true === $the_user) {
            import("actions/blog/class.blog");
            $blog = new Blog();
            $blogid = $blog->create($_SESSION["member"]["uid"], $sub_domain);
            $_SESSION["member"]["blogs"][] = $blog->get_by($blogid);
            r("home");
        } else {
            $errors[$the_user[0]] = $the_user[1];
        }
    }
}

if($errors) {
    $tpl->assign("errors", $errors);
}

$tpl->display("account/register.tpl");