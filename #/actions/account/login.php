<?php

if($user->is_login) {
    r("home");
}

if(is_post()) {
    $email = trim($_POST["email"]);
    $pwd  = trim($_POST["password"]);
    $remember_me = $_POST["remember_me"] ? true : false;
    $tpl->assign("the_email", $email);
    if(!is_email($email)) {
        $errors['email'] = "邮箱格式不对";
    }
    
    if(strlen($pwd) < 6) {
        $errors["pwd"] = "密码不能小于6位";
    }
    
    if(!$errors) {
        $the_user = $user->login($email, $pwd, $remember_me);
        if(true === $the_user) {
            if($_GET['next']) {
                r(urldecode($_GET["next"]));
            } else {
                r("home");
            }
        } else {
            $errors[$the_user[0]] = $the_user[1];
        }
    }
    
}
if($errors) {
    $tpl->assign("errors", $errors);
}
$tpl->display("account/login.tpl");