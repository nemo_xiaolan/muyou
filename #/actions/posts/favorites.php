<?php

/**
 * 我喜欢的文章
 */

need_login();

$tpl->assign("the_blog", $_SESSION["member"]["blogs"][0]);

$tpl->assign("is_fav_page", true);

//the_blog_posts

import("actions/posts/class.posts");
$post = new Posts();
$paginator = $post->get_favorites($user->uid);
$tpl->assign("the_blog_posts", $paginator->items);
$tpl->assign("paginator", $paginator);

$tpl->assign("page_title", "我喜欢的");
$tpl->display("blog/posts/favorites.tpl");