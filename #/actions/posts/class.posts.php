<?php

class Posts extends BaseModel {
    
    public $table = "blog_posts";
    
    /**
     * 获取单条信息
     * @param integer $id
     * @param integer $uid //访问者ID
     */
    public function get($id, $uid) {
        $the_post = $this->get_by($id);
        if(!$the_post) {
            return array();
        }
        
        import("actions/posts/class.favorite");
        $fav = new Favorite();
        $the_post["is_fav"] = $fav->is_favorited($id, $uid);
        return $the_post;
    }
    
    /**
     * 获取我喜欢的
     */
    public function get_favorites($uid, $offset = 20) {
        $fav_table = $this->get_table("favorites");
        $blog_table = $this->get_table("blogs");
        $total = $this->count(sprintf(" WHERE uid = %d", $uid), "postid", "favorites");
        $sql = "SELECT f.postid, f.dateline, p.*, b.avatar, b.name as blog_name, b.sub_domain, b.uid AS blog_manager
                FROM {$fav_table} f
                LEFT JOIN {$this->table} p ON p.id = f.postid
                LEFT JOIN {$blog_table} b ON b.id = p.blogid
                WHERE f.uid = %d";
                
        $sql = sprintf($sql, $uid);
        
        import("lib/class.paginator");
        $p = new Paginator($this->db, $_GET["page"], $offset);
        $p->work($sql, $total);
        
        if(!$p->items) {
            return array();
        } else {
            return $this->merge_post_meta($p, $uid);
        }
    }
    
    /**
     * 获取某个博客的内容
     */
    public function get_posts_by_blog($blogid, $offset=20) {
        
        $blog_table = $this->get_table("blogs");
        $condition = sprintf(" WHERE bp.blogid = %d", $blogid);
        $total = $this->count(str_replace("bp.blogid", "blogid", $condition));
        
        $sql = "SELECT bp.*, b.avatar, b.name as blog_name, b.sub_domain, b.uid AS blog_manager
                FROM {$this->table} bp
                LEFT JOIN {$blog_table} b ON b.id = bp.blogid
                {$condition}
                ORDER BY bp.id DESC";
                
        import("lib/class.paginator");
        $p = new Paginator($this->db, $_GET["page"], $offset);
        $p->work($sql, $total);
        if(!$p->items) {
            return array();
        } else {
            return $this->merge_post_meta($p, $uids[0]);
        }
    }
    
    /**
     * 取得个人首页要显示的文章
     * @param integer $uid
     */
    public function get_index_posts($uids, $offset=20, $merge_following=true) {
        $uids = is_array($uids) ? $uids : array($uids);
        //$following = array();
        if($merge_following) {
            import("actions/blog/class.follow");
            $follow = new Follow();
            $following = $follow->get_following_ids($uids[0]);
            $uids = array_merge($uids, $following);
        }
        if(!$uids) {
            return array();
        }
        
        $condition = sprintf(" WHERE bp.uid IN(0,%s)", implode(",", $uids));
        $total = $this->count(str_replace("bp.uid", "uid", $condition));
        $blog_table = $this->get_table("blogs");
        
        $sql = "SELECT bp.*, b.avatar, b.name as blog_name, b.sub_domain, b.uid AS blog_manager
                FROM {$this->table} bp
                LEFT JOIN {$blog_table} b ON b.id = bp.blogid
                {$condition}
                ORDER BY bp.id DESC";
        
                
        import("lib/class.paginator");
        $p = new Paginator($this->db, $_GET["page"], $offset);
        $p->work($sql, $total);
        if(!$p->items) {
            return array();
        } else {
            return $this->merge_post_meta($p, $uids[0]);
        }
    }
    
    /**
     * 根据Ids
     */
    public function get_by_ids($ids, $uid) {
        $p = $this->get_page_list(array(
            "id" => sprintf("^(0, %s)", implode(",", $ids))
        ));
        return $this->merge_post_meta($p, $uid);
    }
    
    
    /**
     * 获取并合并博文的其他信息到分页数组中
     * eg: tag, is_like, 
     * @param object $p => Paginator
     */
    private function merge_post_meta($p, $uid = null) {
        $itemids = array();
        $the_items = array();
        foreach($p->items as $item) {
            $itemids[] = $item["id"];
            $the_items[$item["id"]] = $item;
        }
        
        //获取tag
        import("actions/tags/class.tags");
        $t = new Tags();
        $all_tags = $t->get_tags_by_itemids($itemids, "post");
        if($all_tags) {
            foreach($all_tags as $at) {
                $the_items[$at["itemid"]]["the_tags"][] = array(
                    "id"   => $at["tagid"],
                    "name" => $at["name"],
                    "blogid"  => $at["blogid"]
                );
            }
        }
        
        if($uid) {
            /**
            * 获取是否喜欢
            */
            import("actions/posts/class.favorite");
            $fav = new Favorite();
            $the_favs = $fav->get_is_fav_by_ids($itemids, $uid);
            foreach($the_favs as $k=> $v) {
                $the_items[$k]["is_fav"] = $v;
            }
        }
        
        $p->items = $the_items;
        return $p;
    }
    
    
    
    /**
     * 发布一篇内容
     * @param array $data 中间必须要包含一个type的参数
     * @return integer last_insert_id();
     */
    public function publish($data = array()) {
        if(!is_array($data)) {
            $data = array("type" => $data);
        }
        $method = "publish_".$data['type'];
        global $user;
        $datas = array(
            "uid" => $user->uid,
            "blogid" => abs(intval($_POST["post_to"])),
            "subject" => h($_POST['subject']),
            "content" => remove_xss($_POST["text"]),
            "dateline"=> CURRENT_TIMESTAMP,
            "private" => isset($_POST["private"]) ? 1 : 0,
            "toped"   => isset($_POST["toped"]) ? 1 : 0,
            "favorites" => 0,
            "replies" => 0,
            "is_submit" => $data["is_submit"] ? 1 : 0
        );
        
        $datas = array_merge($datas, $data);
        
        /**
         * @todo 验证是否为本人博客
         * @todo 投稿
         * @todo 自定义链接
         */
        
        if(method_exists($this, $method)) {
            return $this->$method($datas);
        } else {
            return false;
        }
    }
    
    /**
     * 发布文字
     * @todo 缩略图/editor 图片
     */
    private function publish_text($datas) {
        return $this->insert($datas);
    }
    
    /**
     * 发布音乐
     */
    private function publish_audio($datas) {
        $datas['thumbnail'] = trim(strip_tags($_POST['thumbnail']));
        $datas['extra'] = serialize(array(
            "songid" => abs(intval($_POST["songid"]))
        ));
        return $this->insert($datas);
    }
    
    /**
     * 发布链接
     */
    private function publish_link($datas) {
        if(is_url($_POST["url"])) {
            $datas["subject"] = sprintf('<a href="%s" target="_blank" class="pb_link">%s</a>', $_POST["url"], $datas["subject"]);
        }
        return $this->insert($datas);
    }
    
    /**
     * 发布视频
     */
    private function publish_video($datas) {
        if(is_url($_POST['flvurl'])) {
            $datas["extra"] = serialize(array(
                "flvurl" => trim(strip_tags($_POST["flvurl"])),
                "source" => trim(strip_tags($_POST["video_url"]))
            ));
        }
        $datas['thumbnail'] = trim(strip_tags($_POST['thumbnail']));
        return $this->insert($datas);
    }
    
    /**
     * 发布照片
     */
    private function publish_photo($datas) {
        if(!$datas["photos"]) {
            return false;
        }
        $photos = $datas["photos"];
        $datas["extra"] = serialize(array(
            "photos_count" => count($datas["photos"])
        ));
        unset($datas["photos"]);
        $postid = $this->insert($datas);
        /**
        * 保存图片到数据库
        */
        $the_photos_values = array();
        foreach($photos as $p) {
            array_push($the_photos_values, 
                    sprintf('(%d, %d, %d, "%s")', 
                            $datas["uid"], $postid, $datas["blogid"], $p
                            )
                    );
        }
        
        $photo_table = $this->get_table("blog_post_photos");
        $sql = "INSERT INTO {$photo_table}(uid, postid, blogid, save_path)VALUES %s";
        $sql = sprintf($sql, implode(",", $the_photos_values));
        
        $this->db->query($sql);
        return $postid;
    }
    
}