<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

need_ajax();
need_login();

$postid = $_POST["postid"];

import("actions/posts/class.favorite");
$fav = new Favorite();
$fav->toggle_favorite($postid, $user->uid);