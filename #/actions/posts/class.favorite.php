<?php

/**
 * 喜欢
 */
class Favorite extends BaseModel {
    
    public $table = "favorites";
    
    /**
     * 根据ids获取一系列的posts是否已经被用户喜欢
     * @param array $ids
     * @param integer $uid
     * @return array
     */
    public function get_is_fav_by_ids($ids, $uid) {
        $the_ids = implode(",", $ids);
        $sql = "SELECT * FROM {$this->table}
                WHERE postid IN(0, %s) AND uid = %d";
        $sql = sprintf($sql, $the_ids, $uid);
        $data = $this->db->query($sql)->fetchAll();
        $favs = array();
        
        if($data) {
            foreach($data as $d) {
                $favs[$d["postid"]] = 1;
            }
        }
        
        $return = array();
        foreach($ids as $id) {
            $return[$id] = $favs[$id];
        }
        
        return $return;
    }
    
    /**
     * 是否喜欢
     */
    public function is_favorited($postid, $uid) {
        $sql = "SELECT * FROM {$this->table} 
                WHERE postid = %d AND uid = %d";
        $sql = sprintf($sql, $postid, $uid);
        return $this->db->query($sql)->fetch();
    }
    
    public function toggle_favorite($postid, $uid) {
        if($this->is_favorited($postid, $uid)) {
            $this->unfav($postid, $uid);
        } else {
            $this->fav($postid, $uid);
        }
    }
    
    public function unfav($postid, $uid) {
        $sql = "DELETE FROM {$this->table}
                WHERE postid = %d AND uid = %d";
        $sql = sprintf($sql, $postid, $uid);
        return $this->db->query($sql);
    }
    
    public function fav($postid, $uid) {
        return $this->insert(array(
            "postid"  => abs(intval($postid)),
            "uid"     => abs(intval($uid)),
            "dateline"=> CURRENT_TIMESTAMP
        ));
    }
    
}