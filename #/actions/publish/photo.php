<?php

/**
 * 发布照片：
 *  流程: 1. swfupload ajax上传保存到tmp目录 生成显示的缩略图 ajax_photo
 *        2 .保存时将图片从tmp目录移动到相应目录， 保存到数据库， 删除原文件 this file
 * 
 * 暂定不生成缩略图 使用url map将图片请求转到posts/rewrite_thumbnail处理输出
 */

need_login();

$domain = $_GET["blog"];
if($domain) {
    import("actions/blog/class.blog");
    $blog = new Blog();
    $the_blog = $blog->get_info($domain);
    $tpl->assign("the_blog", $the_blog);
    $tpl->assign("single_domain", $domain);
}

if(is_post()) {
    
    if($_SESSION["member"]["UPLOAD_TMP"]) {
        
        /**
         * 复制到新的目录并且
         * 删除tmp目录下的相应图片并unset session中图片数据
         */
        $dir = "attach/photos/".date("Ymd");
        mk_dir($dir);
        $photos = array();
        foreach($_SESSION["member"]["UPLOAD_TMP"] as $tmp) {
            $filename = end(explode("/", $tmp));
            if(!is_file(ENTRY_DIR.DS.$tmp)) {
                continue;
            }
            @copy(ENTRY_DIR.DS.$tmp, ENTRY_DIR.DS.$dir.DS.$filename);
            @unlink(ENTRY_DIR.DS.$tmp);
            
            $photos[] = $dir."/".$filename;
        }
        
        /**
         * 保存文章
         */
        import("actions/posts/class.posts");
        $post = new Posts();
        $postid = $post->publish(array(
            "type" => "photo",
            "photos" => $photos
        ));
        
        if($postid) {
            /**
             * 保存TAG
             */
            if($_POST['post_tags']) {
                import("actions/tags/class.tags");
                $t = new Tags();
                $t->set_tag($postid, abs(intval($_POST["post_to"])), $_POST['post_tags'], "post");
            }
            
            unset($_SESSION["member"]["UPLOAD_TMP"]);

            r("home");
        }
    }
}

unset($_SESSION["member"]["UPLOAD_TMP"]);

$tpl->assign("session_id", session_id());
$tpl->display("publish/photo.tpl");