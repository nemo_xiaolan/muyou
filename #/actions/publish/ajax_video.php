<?php

if(!is_ajax()) {
    r("home");
}

$video = urldecode($_GET["v"]);

if(!is_url($video)) {
    return false;
}

$the_video = parseflv($video);

if($the_video) {
    echo json_encode($the_video);
}