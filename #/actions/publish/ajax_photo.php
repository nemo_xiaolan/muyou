<?php

if(!is_ajax()) {
    //r("home");
}

if(!$_GET["UPLOAD_HASH"] or !$_SESSION["member"]) {
    r("home");
}

import("lib/io/file");
$f = new File();
$save_path = $f->upload("Filedata");

$response = array();
if(!$save_path) {
    $response["error"] = $save_path;
} else {
    if(!is_array($_SESSION["member"]["UPLOAD_TMP"])) {
        $_SESSION["member"]["UPLOAD_TMP"] = array();
    }
    $_SESSION["member"]["UPLOAD_TMP"][] = $save_path;
//    $thumb_dir = ENTRY_DIR.DS.$f->upload_target."/thumb";
//    !is_dir($thumb_dir) && mk_dir($thumb_dir);
//    
//    /**
//     * 生成缩略图
//     */
//    import("lib/io/image");
//    $image = new Image();
//    $image->open(ENTRY_DIR.DS.$save_path);
//    $image->thumbnail(100, 100);
//    $image->save_to($thumb_dir."/".$f->upload_filename);
    
    //$response["thumb"] = str_replace($f->upload_filename, $save_path);
    
    $response["thumb"] = $save_path;
}

echo json_encode($response);

unset($response);
unset($save_path);
unset($f);