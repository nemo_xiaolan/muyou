<?php

/**
 * 发布信息公共
 */

need_login();

$domain = $_GET["blog"];
if($domain) {
    import("actions/blog/class.blog");
    $blog = new Blog();
    $the_blog = $blog->get_info($domain);
    $tpl->assign("the_blog", $the_blog);
    $tpl->assign("single_domain", $domain);
}

if(is_post()) {
    
    import("actions/posts/class.posts");
    $post = new Posts();
    $postid = $post->publish($action["not_found"]);
    
    if($postid) {
        /**
        * 保存TAG
        */
        if($_POST['post_tags']) {
            import("actions/tags/class.tags");
            $t = new Tags();
            $t->set_tag($postid, abs(intval($_POST["post_to"])), $_POST['post_tags'], "post");
        }
    }
    
    r("home");
}

$tpl->display("publish/{$action["not_found"]}.tpl");