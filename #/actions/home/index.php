<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */
if($user->is_login) {
    $tpl->assign("the_blog", $_SESSION["member"]["blogs"][0]);
    
    import("actions/posts/class.posts");
    $posts = new Posts();
    $index_posts = $posts->get_index_posts($user->uid, 20);
    
    $tpl->assign("is_home", true);
    $tpl->assign("paginator", $index_posts);
    $tpl->display("home/index.tpl");
} else {
    import("actions/account/register");
}