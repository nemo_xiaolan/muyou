<?php

/**
 * 博客分类页面
 */

if(isset($action["not_found"])) {
    $catid = $action["not_found"] ? abs(intval($action["not_found"])) : 1;
} else {
    $catid = 1;
}

import("actions/category/class.category");
$category = new Category();
$the_categories = $category->get_list("", "orderlist DESC, id ASC");

$the_category = $category->get_by($catid);

import("actions/blog/class.blog");
$blog = new Blog();
$the_blogs = $blog->get_list_by_catid($catid, $user->uid);

$tpl->assign("page_title", sprintf("%s博客", $the_category["name"]));
$tpl->assign("blogs_list", $the_blogs);
$tpl->assign("current_id", $catid);
$tpl->assign("the_categories", $the_categories);
$tpl->display("category/index.tpl");