<?php

/**
 * 取消关注
 */

need_login();

$domain = $action["params"][0];

if(!$domain) {
    r("home");
}

import("actions/blog/class.blog");
import("actions/blog/class.follow");

$blog = new Blog();
$the_blog = $blog->get_info($domain);

if(!$the_blog) {
    r("home");
}

$follow = new Follow();
$follow->unfollow($the_blog["id"], $user->uid);

r($_SERVER["HTTP_REFERER"]);