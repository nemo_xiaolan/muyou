<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

need_login();

import("actions/blog/class.follow");

$f = new Follow();
$the_blogs = $f->get_following($user->uid);
$tpl->assign("the_blogs_list", $the_blogs->items);
$tpl->assign("paginator", $the_blogs);

$tpl->assign("is_follow_page", true);
$tpl->display("blog/following.tpl");