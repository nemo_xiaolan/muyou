<?php

/**
 * 单独某个博客的发布主页
 */
need_login();

$domain = $action["not_found"];
if(!$domain) {
    r("home");
}

/**
 * 当前的blog信息
 */
import("actions/blog/class.blog");
$blog = new Blog();
$the_blog = $blog->get_by($domain, "sub_domain");
if($the_blog["uid"] != $user->uid) {
    r("home");
}

$tpl->assign("the_blog", $the_blog);

import("actions/posts/class.posts");
$posts = new Posts();
$index_posts = $posts->get_posts_by_blog($the_blog["id"], 20, false);
$tpl->assign("paginator", $index_posts);

$tpl->assign("single_domain", $domain);
$tpl->assign("domain_url", u("blog/$domain")."/");

$tpl->assign("page_title", $the_blog["name"]);
$tpl->display("blog/single.tpl");