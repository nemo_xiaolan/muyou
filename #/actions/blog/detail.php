<?php

/**
 * 博客主页面
 */

$domain = $_GET["blog"];
if(!$domain) {
    r("home");
}

import("actions/blog/class.blog");
import("actions/posts/class.posts");

$blog = new Blog();
$the_blog = $blog->get_info($domain);
if(!$the_blog) {
    r("home");
}

if($_GET["preview"]) {
    $theme = $_GET["preview"];
} else {
    $theme = $the_blog["theme"];
}

$theme_dir = sprintf(ENTRY_DIR."%s__%s%s", DS, DS, $theme);

if(!is_dir($theme_dir)) {
    $theme_dir = sprintf(ENTRY_DIR."%s__%sdefault", DS, DS);;
}

$tpl->template_dir = array(
        $tpl->template_dir[0],
        $theme_dir
    );

/**
 * 模板配置
 */
if(is_file($theme_dir.'/config.php')) {
    $theme_config = require($theme_dir."/config.php");
} else {
    $theme_config = array(
        "列表文章数量" => 20
    );
}

/**
 * 博客基本信息
 */
$tpl->assign("the_blog", $the_blog);
$tpl->assign("theme_config", $theme_config);
if($the_blog["uid"] == $user->uid) {
    $is_manager = true;
    $tpl->assign("is_manager", $is_manager);
}

/**
 * @todo 获得标签云
 */

/**
 * 是否已关注
 */
import("actions/blog/class.follow");
$follow = new Follow();
$tpl->assign("is_following", $follow->is_following($the_blog["id"], $user->uid));

$posts = new Posts();

/**
 * 内页
 */
list($the_action, $the_id) = $action["params"];

switch($the_action) {
    case "posts":
        import("actions/blog/post");
        break;
    default:
        $the_posts = $posts->get_page_list(array(
            "blogid" => $the_blog["id"]
        ), 'id DESC', $theme_config["列表文章数量"]);
        
        /**
        * 博客文章信息
        */
        $tpl->assign("the_posts", $the_posts->items);
        $tpl->assign("paginator", $the_posts);
        $tpl->display("template/blog_index.tpl");
}