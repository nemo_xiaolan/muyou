<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */
/**
 * 创建新的博客
 */
need_login();

/**
 * 最大博客数量
 */
$max_blog_num = 3;

import("actions/blog/class.blog");
$blog = new Blog();
$total = $blog->count(sprintf(" WHERE uid = %d", $user->uid));

if($total >= $max_blog_num) {
    $tpl->assign("maxed", true);
    $tpl->display("blog/new.tpl");
} else {
    
    if(is_post()) {
        
    } else {
        $tpl->display("blog/new.tpl");
    }
    
}