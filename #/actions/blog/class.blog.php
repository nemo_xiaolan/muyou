<?php

class Blog extends BaseModel {
    
    public $table = "blogs";
    
    public $denied_blog_domain = array(
        "admin", "administrator", "guanli", "new", "posts", "houtai", "manage", "fuck"
    );
    
    public function get_list_by_catid($catid, $uid = 0) {
        
        $blogs = $this->get_list(array("catid"=> $catid));
        if(!$blogs) {
            return array();
        }
        
        if(!$uid) {
            return $blogs;
        }
        
        $the_blogs = array();
        $the_blogs_ids = array();
        foreach($blogs as $k=>$v) {
            $the_blogs[$v["id"]] = $v;
            $the_blogs_ids[] = $v["id"];
        }
        
        import("actions/blog/class.follow");
        $follow = new Follow();
        $followed = $follow->get_following_by_blogids($the_blogs_ids, $uid);
        
        if(!$followed) {
            return $the_blogs;
        }
        
        foreach($the_blogs as $k=> $blog) {
            if(key_exists($k, $followed)) {
                $the_blogs[$k]["is_following"] = $followed[$k];
            } else {
                $the_blogs[$k]["is_following"] = 0;
            }
        }
        
        
        return $the_blogs;
    }
    
    /**
     * 创建一个博客
     * @param $uid
     * @param $sub_domain
     * @return integer last insert id
     */
    public function create($uid, $sub_domain) {
        if(!preg_match("/[a-z0-9\-]{5,15}/", $sub_domain)) {
            return array("sub_domain", "个性域名格式不对");
        }
        if($this->get_by($sub_domain, "sub_domain") 
                or in_array($sub_domain, $this->denied_blog_domain)) {
            return array("sub_domain", "个性域名已经被使用了");
        }
        
        return $this->insert(array(
            "uid" => $uid,
            "name" => $sub_domain,
            "sub_domain" => $sub_domain
        ));
    }
    
    /**
     * 获取博客信息
     */
    public function get_info($domain) {
        return $this->get_by($domain, "sub_domain");
    }
    
}