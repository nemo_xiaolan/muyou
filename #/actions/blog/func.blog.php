<?php

/**
 * 返回用户的Blog地址
 * @todo 对二级子域名和独立域名的支持
 * @todo 
 */
function blog_url($domain, $action="") {
    global $C;
    $action = $action ? "/".$action : $action;
    $host = str_replace("www.", "", $C["base"]["host"]);
    return "http://".$domain.'.'.$host.$action;
}

/**
 * @todo 返回用户头像的地址
 */
function avatar($avatar, $size="30") {
    if(!$avatar) {
        return "_/public/images/default_avatar_{$size}.jpg";
    }
}

/**
 * 返回博文地址
 */
function post_url($id, $sub_domain) {
    
}