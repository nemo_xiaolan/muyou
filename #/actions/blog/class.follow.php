<?php

class Follow extends BaseModel {
    
    public $table = "follow";
    
    /**
     * 关注某博客
     * @param integer $blogid 被关注博客ID
     * @param integer $followerid 关注者ID
     */
    public function with($blogid, $followerid) {
        
        if($this->is_following($blogid, $followerid)) {
            return true;
        }
        
        return $this->insert(array(
            "followed" => $blogid,
            "follower" => $followerid,
            "dateline" => CURRENT_TIMESTAMP
        ));
    }
    
    /**
     * 取消关注
     */
    public function unfollow($blogid, $followerid) {
        $sql = "DELETE FROM {$this->table} WHERE follower=:follower AND followed=:followed";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array(
            ":follower" => $followerid,
            ":followed" => $blogid
        )); 
   }
    
    /**
     * 是否已经关注
     */
    public function is_following($blogid, $followerid) {
        $sql = "SELECT * FROM {$this->table} WHERE follower=:follower AND followed=:followed";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(
            ":follower" => $followerid,
            ":followed" => $blogid
        ));
        return $stmt->fetch();
    }
    
    /**
     * 获取关注的博客ID
     */
    public function get_following_ids($uid) {
        $sql = "SELECT followed FROM {$this->table} WHERE follower=:follower";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(
            ":follower" => $uid
        ));
        $data = $stmt->fetchAll();
        $ids = array();
        if($data) {
            foreach($data as $d) {
                $ids[] = $d["followed"];
            }
        }
        
        return $ids;
    }
    
    /**
     * 获取博客列表是否关注
     */
    public function get_following_by_blogids($blogids, $uid) {
        $sql = "SELECT * FROM {$this->table} WHERE follower = %d AND followed IN (0, %s)";
        $sql = sprintf($sql, $uid, implode(",", $blogids));
        
        $data = $this->db->query($sql)->fetchAll();
        
        if(!$data) {
            return array();
        }
        
        $return = array();
        foreach($data as $d) {
            $return[$d["followed"]] = $d["follower"];
        }
        
        return $return;
    }
    
    /**
     * 获取关注的博客列表
     */
    public function get_following($uid, $size=10) {
        
        $blog_table = $this->get_table("blogs");
        $sql = "SELECT b.* FROM {$this->table} f
                LEFT JOIN {$blog_table} b ON b.id = f.followed
                WHERE f.follower = %d";
                
        $sql = sprintf($sql, $uid);
        $total = $this->count(" WHERE follower = ".$uid, "followed");
        
        import("lib/class.paginator");
        $p = new Paginator($this->db, $_GET["page"], $size);
        return $p->work($sql, $total);
    }
    
}