<?php

/**
 * 博客文章详情页
 */

import("actions/blog/class.blog");
$posts = new Posts();

list(, $the_id) = $action["params"];
$the_post = $posts->get($the_id, $user->uid);
$tpl->assign("page_title", $the_post["subject"]);
$tpl->assign("the_post", $the_post);
$tpl->display("template/blog_post.tpl");