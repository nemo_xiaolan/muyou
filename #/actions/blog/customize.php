<?php

/**
 * 博客设置
 */

need_login();

$domain = $action["params"][0];

if(!$domain) {
    $domain = $_SESSION["member"]["blogs"][0]["sub_domain"];
}

import("actions/blog/class.blog");
$blog = new Blog();
$the_blog = $blog->get_info($domain);

if(!$the_blog or $the_blog["uid"] != $user->uid) {
    r("home");
}

$tpl->assign("page_title", "博客设置");
$tpl->assign("the_blog", $the_blog);
$tpl->display("blog/customize.tpl");