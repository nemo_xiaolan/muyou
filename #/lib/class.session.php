<?php

class Session extends BaseModel {

    public $table = "session";

    public $sess_instance;

    public $life = 86400;
    
    public function __construct() {
        parent::__construct();
        session_set_save_handler(
            array(&$this, "open"),
            array(&$this, "close"),
            array(&$this, "read"),
            array(&$this, "write"),
            array(&$this, "destroy"),
            array(&$this, "gc")
        );
    }
    
    public function open($save_path, $sess_name) {
        return true;
    }

    public function close() {
        return true;
    }

    public function read($key) {
        $sql = "SELECT value FROM {$this->table} 
                WHERE sesskey = '%s' AND expiry > %d LIMIT 1";
        $sql = sprintf($sql, $key, CURRENT_TIMESTAMP);
        $session = $this->db->query($sql)->fetch();
        return $session["value"];
    }

    public function write($key, $value) {
        $expiry = CURRENT_TIMESTAMP+$this->life;
        return $this->replace(array(
            "sesskey" => $key,
            "expiry"  => $expiry,
            "value"   => $value
        ), $key, "sesskey");
    }
    
    public function destroy($key) {
        return $this->delete($key, "sesskey");
    }
    
    public function gc($maxlife) {
        $sql = "DELETE FROM {$this->table}
                WHERE expiry < %d";
        $sql = sprintf($sql, CURRENT_TIMESTAMP);
        return $this->db->exec($sql);
    }
}

new Session();