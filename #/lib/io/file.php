<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

class File {
    
    /**
     * 上传保存在哪
     */
    public $upload_target = "attach/tmp";
    
    /**
     * 对文件名进行hash
     */
    public $upload_hash_filename = "sha1";

    /**
     * 允许上传的类型
     */
    public $upload_allow_type = array(
        "image" => array("jpg", "jpeg", "gif", "png")
    );
    
    /**
     * 文件最大值 KB
     */
    public $upload_max_size = 4096;
    
    /**
     * 错误信息
     */
    public $upload_erros = array();
    
    /**
     * 保存的文件名
     */
    public $upload_filename;
    
    /**
     * 保存到的相对路径
     */
    public $upload_saved_path;
    
    /**
     * File::upload
     * @param string $source $_FILES[$soure]
     * @return string $http_path relative path of http
     */
    public function upload($source, $allow = "image") {
        $source = $_FILES[$source];
        if($source["error"]) {
            $this->upload_error = "upload_error";
            return false;
        }
        if(!$this->check_upload_type($source, $allow)) {
            $this->upload_error = "upload_type_denied";
            return false;
        }
        if(!$this->check_upload_size($source)) {
            $this->upload_error = "upload_to_big";
            return false;
        }
        
        $upload_target = ENTRY_DIR.DS.$this->upload_target;
        if(!is_dir($upload_target) and !mk_dir($upload_target)) {
            $this->upload_error = "upload_target_not_exists";
            return false;
        }
        
        $ext = strtolower(end(explode(".", $source["name"])));
        $func = $this->upload_hash_filename;
        $this->upload_filename = $this->upload_hash_filename ? $func(time().uniqid()).".".$ext : $filename;
        if(move_uploaded_file($source["tmp_name"], sprintf("%s/%s", $upload_target, $this->upload_filename))) {
            return sprintf("%s/%s", $this->upload_target, $this->upload_filename);
        } else {
            $this->upload_error = "upload_target_not_exists";
            return false;
        }
    }
    
    private function check_upload_type($file, $allow = "image") {
        $ext = strtolower(end(explode(".", $file["name"])));
        return in_array($ext, $this->upload_allow_type[$allow]) and @getimagesize($file["tmp_name"]);
    }
    
    private function check_upload_size($file) {
        return $file["size"] <= $this->upload_max_size*1024;
    }
    
}