<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */
!defined("IN_APP") && exit();

function compress_html_and_output() {
    $the_contents = ob_get_clean();
    $the_contents = explode("\n", $the_contents);
    foreach($the_contents as $line) {
        //$contents.= trim($line)."\n";
        $contents .= $line."\n";
    }
    //$contents = preg_replace(array("/([>;]+)\\n/"), array("$1"), $contents);
    unset($the_contents);
    Pluggable::trigger("after_template_render");
    echo $contents;
}

register_shutdown_function('compress_html_and_output');