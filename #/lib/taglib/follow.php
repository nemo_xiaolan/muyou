<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

!defined("IN_APP") and exit();
/**
 * 获取我跟随的博客数量
 */
function smarty_function_following_num($params, &$smarty) {
    global $user;
    if(!$user->uid) {
        return 0;
    }
    
    import("actions/blog/class.follow");
    
    $follow = new Follow();
    return count($follow->get_following_ids($user->uid));
}


$tpl->registerPlugin("function", "following_count", "smarty_function_following_num");