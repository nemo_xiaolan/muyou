<?php

/**
 * 获取我喜欢的文章数量
 */
function my_favorites_count($params, &$smarty) {
    import("actions/posts/class.favorite");
    $fav = new Favorite();
    global $user;
    return $fav->count(array("uid" => $user->uid), "postid");
}

$tpl->registerPlugin("function", "my_favorites_count", "my_favorites_count");