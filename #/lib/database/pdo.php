<?php

/**
 * @package lib.org.database.pdo
 */
class PdoBackend {
    
    static protected $instance = null;
    
    /**
     * PdoBackend::init()
     * 
     * @return object instance of PDO
     */
    static public function get_instance() {
        
        if(self::$instance) {
            return self::$instance;
        }
        
        global $C;
        try {
            self::$instance = new PDO($C['database']['dsn'], $C['database']['usr'], $C['database']['pwd']);
            self::$instance->exec('SET NAMES UTF8');
            self::$instance->exec('SET AUTOCOMMIT = 1');
        } catch(PDOException $e) {
            send_http_status(500, true);
        }
        
        return self::$instance;
    }
}

?>
