<?php

/**
 * 导入包
 * 可替代require和require once
 */
function import($package) {
    static $imported_files = array();
    $realpath = PROJECT_DIR.DS.str_replace("/", DS, $package).".php";
    
    if(!key_exists($package, $imported_files)) {
        $imported_files[$package] = $realpath;
        global $C;
        global $user;
        global $action;
        global $tpl;
        return require $realpath;
    }
}

/**
 * 载入Smarty插件
 */
function load_smarty_tag($dir = null) {
    $dir = $dir ? $dir : PROJECT_DIR."/lib/taglib";
    $handler = opendir($dir);
    if($handler) {
        global $tpl;
        while (($filename = readdir($handler)) !== false) {
            if($filename != '.' and $filename != '..' and is_file($dir.DS.$filename)) {
                require $dir.DS.$filename;
            }
        }
        closedir($handler);
    }
}