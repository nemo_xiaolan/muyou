<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 * @package #.lib.dispatcher.dispatcher
 * 有两种模式
 *  1. normal: index.php?-=home/index
 *  2. pathinfo: /home/list/1.html?page=2
 */

class Dispatcher {
    
    static public $type = "normal";
    
    static public $delimiter = "?-=";
    
    static public $extra = "";
    
    static public $is_maped = false;
    
    static public function work() {
        list($uri, $params) = explode("?", $_SERVER["REQUEST_URI"]);
        /**
         * 初始GET参数
         */
        $params = explode("&", $params);
        foreach($params as $param) {
            list($k, $v) = explode("=", $param);
            $_GET[$k] = $v;
        }
        
        /**
         * 首先检测URL Map
         */
        $map = self::check_url_map($uri);
        if(false !== $map) {
            $uri = $map;
            /**
             * Map 之后的GET参数
             */
            list($uri, $params) = explode("?", $uri);
            $params = explode("&", $params);
            foreach($params as $param) {
                list($k, $v) = explode("=", $param);
                $_GET[$k] = $v;
            }
        }
        
        /**
         * Normal 模式
         */
        if(strpos($uri, self::$delimiter)) {
            self::$type = "normal";
            $action = self::normal($uri);
        /**
         * Pathinfo模式
         */
        } else {
            $action = self::pathinfo($uri);
        }
        
        return $action;
    }
    
    /**
     * 实现URL Map 正则需使用命名组 命名组只支持小写字母
     * @param $uri 当前请求URI
     */
    static public function check_url_map($uri) {
        $server = str_ireplace("index.php", "", $_SERVER["PHP_SELF"]);
        $uri = substr($uri, strlen($server));
        $urlmap = require PROJECT_DIR.DS."urlmap.php";
        foreach($urlmap as $k=> $v) {
            preg_match_all("/$k/", $uri, $matches);
            if(!$matches[0]) {
                continue;
            }
            foreach($matches as $k=> $match) {
                if(!preg_match("/[a-zA-Z_]+/", $k)) {
                    continue;
                }
                $search[] = ":".$k;
                $replace[] = $match[0];
            }
            
            self::$is_maped = true;
            return str_replace($search, $replace, $v);
        }
        
        return false;
    }
    
    static private function normal($uri) {
        if(!self::$is_maped) {
            $uri = end(explode(self::$delimiter, $uri));
        }
        
        list($action, $params) = explode("?", $uri);
        $action = explode("/", $action);
        $group = array_shift($action);
        $name = array_shift($action);
        
        return array(
            "group" => $group,
            "name"  => $name,
            "params"=> $action,
            "get" => $params
        );
    }
    
    /**
     * Pathinfo模式URI
     * eg: publish/text?blog=1
     */
    static private function pathinfo($uri) {
        
        if(!self::$is_maped) {
            $server = str_ireplace("index.php", "", $_SERVER["PHP_SELF"]);
            $uri = substr($uri, strlen($server));
        }
        
        list($action, $params) = explode("?", $uri);
        
        $action = explode("/", $action);
        
        $group = array_shift($action);
        $name = array_shift($action);
        
        return array(
            "group" => $group,
            "name"  => $name,
            "params"=> $action
        );
    }
    
    static public function get_url($action) {
        global $C;
        switch(self::$type) {
            case "normal":
                $uri = self::$delimiter.$action;
                break;
            case "pathinfo":
                $uri = $action;
                break;
        }
        return $uri.self::$extra;
    }
    
}

/**
 * Shortcut
 */
if(!function_exists("u")) {
    function u($action) {
        return Dispatcher::get_url($action);
    }
}