<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */
function ob_gzip($content) {   
    if(!headers_sent() && extension_loaded('zlib')  
            && !empty($_SERVER['HTTP_ACCEPT_ENCODING']) 
            && strstr($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')
            ) {
        $content = gzencode($content, 1);
        //header('Content-Encoding: gzip');
        header('Vary: Accept-Encoding');
        header('Content-Length: '.strlen($content));
    }else{
        header('Content-Length: '.strlen($content));
    }
    return $content;
}