<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Content-Language" content="zh-CN"/>
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <meta name="robots" content="index, follow" />
        <meta name="Description" content="木有啦是一个简单的轻博客，让你能简单快速地发布文字、图片、视频等各种格式内容，通过不同的风格展示来表现你的兴趣或性格。" />
        <meta name="Keywords" content="木有,木有啦,木有博客,木有轻博客" />
        <title>{%if $the_post.subject%}{%$the_post.subject%} - {%/if%}{%$the_blog.name%}</title>
        <base href="{%$C['base']['root']%}" />
        <link rel="stylesheet" type="text/css" href="_/public/css/base.css" />
        <link rel="stylesheet" type="text/css" href="_/public/css/ui.css" />
        {%block main_style%}
        {%/block%}
        {%block extra_style%}{%$smarty.block.child%}{%/block%}
        <script type="text/javascript">
            var BASE_URL = '{%$C['base']['root']%}';
        </script>
        <style type="text/css">
            .btn-panel { list-style-type:none; position: fixed;_position:absolute; z-index:100; right:10px; top:10px; margin:0; padding:0; }
            .btn-panel li { float:right; display:inline; margin:0 0 0 5px; padding:0; }
            .btn-panel a.icon { display:block; overflow:hidden; height:26px; text-indent:-9999px; background-repeat:no-repeat; background-image:url('http://s.libdd.com/img/theme/common/toolbar.$5513.png'); _background-image:url('http://s.libdd.com/img/theme/common/toolbar-ie.$5513.png'); opacity: 0.75; filter:alpha(opacity=75); }
            .btn-panel a.icon:hover { opacity: 1; filter:alpha(opacity=100); }
            a.btn-home { width:82px; background-position:-164px 0; }
            a.btn-register { width:114px; background-position:-780px 0; }
            a.btn-follow { width:62px; background-position:-708px 0; }
            a.btn-fav { width:62px; background-position:-564px 0; }
            a.btn-unfav { width:62px; background-position:-636px 0; }
            a.btn-reblog { width:62px; background-position:0 0; }
            a.btn-edit { width:62px; background-position:-348px 0; }
            a.btn-delete { width:62px; background-position:-420px 0; }
            a.btn-recommend { width:62px; background-position:-492px 0; }
            a.btn-share { width:62px; background-position:-904px 0; }
            a.btn-unfollow { width:82px; background-position:-72px 0; }
            a.btn-customize { width:82px; background-position:-256px 0; }
            .share-container { position: fixed;_position:absolute; top:0; display:none; z-index:200; }
            .share-panel { position:relative; zoom:1; height:26px; width:212px; background:url('http://s.libdd.com/img/theme/common/toolbar.$5513.png') -976px 0 no-repeat; _background:url('http://s.libdd.com/img/theme/common/toolbar-ie.$5513.png') -976px 0 no-repeat; }
            .share-panel a.icon { position:absolute; width:22px; height:22px; top:2px; overflow:hidden; text-indent:-9999px; }
            .share-panel a.icon-sina { left:64px; }
            .share-panel a.icon-tencent { left:87px; }
            .share-panel a.icon-qzone { left:111px; }
            .share-panel a.icon-renren { left:135px; }
            .share-panel a.icon-kaixin { left:159px; }
            .share-panel a.icon-douban { left:182px; }
        </style>
        <script type="text/javascript" src="_/vendor/jquery/jquery.min.js?ver=1.7.1"></script>
    </head>
    <body>
        {%assign var="the_blogid" value=$the_blog.id%}
        {%assign var="the_blog_domain" value=$the_blog.sub_domain%}
        <ul class="btn-panel">
            <li>
                <a target="_top" class="icon btn-home" href="{%"home"|u%}">返回首页</a>
            </li>
            {%if $is_manager%}
                <li>
                    <a target="_top" class="icon btn-customize" href="{%"customize/$the_blog_domain"|u%}">博客设置</a>
                </li>
            {%else%}
                {%if $is_following%}
                <li>
                    <a class="icon btn-unfollow" href="{%"blog/unfollow/$the_blog_domain"|u%}">取消关注</a>
                </li>
                {%else%}
                <li><a class="icon btn-follow" href="{%"blog/follow/$the_blog_domain"|u%}">关注</a></li>
                {%/if%}
            {%/if%}
        </ul>
        {%block body%}
        {%/block%}
        <script type="text/javascript" src="_/public/js/utils.js"></script>
        <script type="text/javascript" src="_/public/js/ui.js"></script>
        {%block extra_js_quote%}{%/block%}
        <script type="text/javascript">
            $(document).ready(function(){
                {%block extra_jquery%}{%/block%}
                UI.button('.ui-button-placeholder');
            });
        </script>
        {%block before_body_end%}{%/block%}
    </body>
</html>        