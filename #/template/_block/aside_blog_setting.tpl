{%assign var="the_domain" value=$the_blog.sub_domain%}
<ul class="sidemenu sidebox">
    <li class="first  aside-blog-name">
        <a target="_blank" href="{%$the_domain|blog_url%}">
            <span class="aside-icon"></span>
            <span class="aside-domain-title">访问 {%$the_blog.name%}</span>
            <span class="aside-domain-url">{%$the_domain|blog_url|replace:"http://":""%}</span>
        </a>
    </li>
    <li class="sub aside-customize">
        <a href="{%"customize/$the_domain"|u%}">
            <span class="aside-icon"></span>
            博客设置
        </a>
    </li>
</ul>