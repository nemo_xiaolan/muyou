<ul class="sidemenu sidebox">
    <li class="first aside-follow-content{%if $is_follow_page%} active{%/if%}">
        <a href="{%"following"|u%}">
            <span class="aside-icon"></span>
            我关注了{%following_count%}个博客
        </a>
    </li>
    <li class="aside-i-like{%if $is_fav_page%} active{%/if%}">
        <a href="{%'favorites'|u%}">
            <span class="aside-icon"></span>
            我喜欢了<span id="side-fav-count">{%my_favorites_count%}</span>篇文章
        </a>
    </li>
    <li class="sub aside-explore-blog">
        <a href="{%"category"|u%}">
            <span class="aside-icon"></span>
            发现更多好博客
        </a>
    </li>
</ul>