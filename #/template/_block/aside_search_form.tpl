<div class="sidebox" id="side-search-holder" style="z-index: 100;">
    <form id="side-search-form" action="{%"search"|u%}" method="POST">
        <div id="side-search-box">
            <input type="text" name="search_keyword" id="side-search-input" class="input-tip" placeholder="搜索标签" />
            <input type="hidden" value="all_tags" name="search_target" />
        </div>
        <button type="submit" id="side-go-search">搜索</button>
    </form>
    <a id="side-search-show-options" href="javascript:;" onclick="utils.toggle_by_focus($('#side-search-options'), $(this));"></a>
    <div class="pop-menu-list-holder fn-hide" id="side-search-options">
        <div class="pop-menu-list-inner">
            <div class="pop-menu-list-triangle"></div>
            <ul class="pop-menu-list mini" id="side-search-options-list">
                <li data="all_tags" class="item-allTag first on"><a>搜索所有标签<span class="pop-menu-extra item-checked"></span></a></li>
                <li data="my_fav" class="item-myTrack"><a>搜索我关注的文章<span class="pop-menu-extra"></span></a></li>
                <li data="my_posts" class="item-myPost"><a>搜索我发布的文章<span class="pop-menu-extra"></span></a></li>
                <li data="i_like" class="item-myFav last"><a>搜索我的喜欢<span class="pop-menu-extra"></span></a></li>
            </ul>
        </div>
    </div>
</div>
<ul class="sidemenu sidebox js-hook-track-tag side-tracked-tag">
    <li class="sub aside-explore-wall first last">
        <a href="{%"explore"|u%}"><span class="aside-icon"></span>探索{%$C["base"]["site_name"]%}</a>
    </li>
</ul>