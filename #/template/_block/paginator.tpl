{%if $paginator->items%}
<div id="paginator" class="pagination">
    <ul>
        {%if $paginator->has_previous%}
            <li class="prev"><a href="{%$paginator->page_param%}{%$paginator->previous%}" class="ui-button-placeholder ui-button-white fn-left">上一页</a></li>
        {%/if%}
        {%if $paginator->has_next%}
            <li class="next"><a href="{%$paginator->page_param%}{%$paginator->next%}" class="ui-button-placeholder ui-button-white fn-right">下一页</a></li>
        {%/if%}
    </ul>
    <div class="clearfix"></div>
</div>
{%/if%}