{%extends "_layouts/base.tpl"%}
{%block "main-wrap"%}
    <div id="main" class="fn-left">
        {%if !$is_fav_page and $user->is_login%}
        <div class="publisher clearfix" id="publisher">
            <div class="pb-avatar">
                <a style="background-image:url({%$the_blog.avatar|avatar:64%})" href="{%$the_blog.sub_domain|blog_url%}" class="blog-avatar">
                    {%$the_blog.name%}
                </a>
            </div>
            <div class="pb-action-holder">
                <div class="pb-triangle"></div>
                <div class="pb-action-shadow-top"></div>
                <div id="pb-action" class="pb-action">
                    <ul class="clearfix">
                        <li><a href="{%$domain_url%}publish/text" class="text">文字</a></li>
                        <li><a href="{%$domain_url%}publish/photo" class="photo">图片</a></li>
                        <li><a href="{%$domain_url%}publish/audio" class="music">声音</a></li>
                        <li><a href="{%$domain_url%}publish/video" class="video">影像</a></li>
                        <li><a href="{%$domain_url%}publish/link" class="link">链接</a></li>
                        <div style="clear:both;"></div>
                    </ul>
                </div>
                <div class="pb-action-shadow-bottom"></div>
            </div>
        </div>
        {%/if%}
        
        <div id="blog-posts-list" {%block blog_posts_list_style%}{%/block%}>
            {%assign var="the_blog_posts" value=$paginator->items%}
            {%include "blog/block_posts_list.tpl"%}
            <div class="blog-post-list-pager">{%include "_block/paginator.tpl"%}</div>
        </div>
    </div>
    
    
    <div id="aside" class="fn-right">
        {%if $user->is_login%}
        {%block aside_all%}
            {%block the_before_aside%}{%/block%}
            {%include "_block/aside_following_and_fav.tpl"%}
            {%include "_block/aside_search_form.tpl"%}
            {%block the_aside%}{%/block%}
        {%/block%}
        {%/if%}
    </div>
    <div class="fn-clear"></div>
{%/block%}