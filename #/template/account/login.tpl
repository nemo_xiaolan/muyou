{%extends "account/layout.tpl"%}

{%block login_form%}
<div id="login-wrap">
    <form method="post" action="" class="clearfix" id="login-form" onsubmit="return check_login_form();">
        <div id="input-login-email" class="input-wrap">
            <span class="input-icon"></span>
            <label>邮箱</label>
            <input type="text" name="email" class="input-text" value="{%$the_email%}" />
            <div {%if !$errors.email%}style="display:none;"{%/if%} class="tip">{%$errors.email|default:"邮箱格式不正确"%}</div>
        </div>
        <div id="input-login-pwd" class="input-wrap">
            <span class="input-icon"></span>
            <label>密码</label>
            <input type="password" name="password" class="input-text">
            <div {%if !$errors.pwd%}style="display:none;"{%/if%} class="tip">{%$errors.pwd|default:"密码最少要六位"%}</div>
        </div>
        <input type="submit" value="登录" class="input-button" id="login-submit">
        
        <div class="clearfix" id="login-help">
            <span id="third-party-login"><a href="{%'account/password/forget'|u%}">忘记密码?</a></span>
            <div class="remember_me fn-right">
                <input type="checkbox" name="remember_me" value="1" checked id="id_remember_me" /> <label for="id_remember_me">记住</label>
            </div>
        </div>
    </form>
</div>
{%/block%}

{%block before_body_end append%}
<script type="text/javascript">
    function check_login_form() {}
</script>
{%/block%}

{%block extra_jquery append%}
    $("input:first").focus();
{%/block%}