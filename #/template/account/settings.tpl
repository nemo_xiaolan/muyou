{%extends "_layouts/base.tpl"%}

{%block main_style append%}
    <link rel="stylesheet" type="text/css" href="_/public/css/settings.css" />
{%/block%}

{%block "main-wrap"%}
<div id="content-holder">
    <div class="setting-header clearfix">
        <h2>设置</h2>
        <div id="setting-goto-btn">
            <a href="{%"blog/customize"|u%}" class="ui-button-placeholder ui-button-blue-mid fn-right">博客设置</a>
            <a href="" class="ui-button-placeholder ui-button-blue-mid fn-right">黑名单</a>
        </div>
    </div>
    {%if $error%}
        <div class="setting-error-msg">{%$error%}</div>
    {%/if%}
    
    <div id="setting-holder">
    <form method="post" action="" id="setting-form" class="setting-form">
        <div id="setting-email" class="setting-item">
            <label>Email:</label>
            <div class="setting-item-main">
                <input type="text" value="{%$user->email%}" readonly name="email" class="ui-input-text skin-text-willwhite" style="width: 330px;" />
            </div>
        </div>
        <div id="setting-current_password" class="setting-item">
            <label>当前密码:</label>
            <div class="setting-item-main">
                <input type="password" value="" name="current_password" class="ui-input-text skin-text-willwhite" style="width: 244px;" />
            </div>
        </div>
        <div id="setting-new_password" class="setting-item">
            <label>新密码:</label>
            <div class="setting-item-main">
                <div>
                <input type="password" value="" name="new_password" class="ui-input-text skin-text-willwhite" style="width: 244px;"  />
                </div>
                <div class="setting-item-option">
                <input type="password" value="" name="new_password_repeat" class="ui-input-text skin-text-willwhite" style="width: 244px;"  />
                </div>
            </div>
        </div>
        <div id="setting-notify" class="setting-item clearfix">
            <label>通知:</label>
            <div class="setting-item-main clearfix">
                <div class="one-blog-setting clearfix">
                    <h5>木有</h5>
                    <div class="one-blog-setting-item clearfix">
                        <label class="label-for-checkbox">
                        <input type="checkbox" checked="true" value="true" name="notIgnoreFollow17939256">
                        博客被关注
                        </label>
                        <label class="label-for-checkbox">
                        <input type="checkbox" checked="true" value="true" name="notIgnoreComment17939256">
                        文章被回应
                        </label>
                        <label class="label-for-checkbox">
                        <input type="checkbox" checked="true" value="true" name="notIgnoreRePost17939256">
                        文章被转载
                        </label>
                        <label class="label-for-checkbox">
                        <input type="checkbox" checked="true" value="true" name="notIgnoreLike17939256">
                        文章被喜欢
                        </label>
                    </div>
                </div>
                <p class="setting-item-tips">勾选后将会收到对应的系统消息</p>
            </div>
        </div>
        <div style="margin:20px 20px 0 0;">
            <button class="ui-button-placeholder ui-button-blue fn-right" onclick="$('#setting-form').submit()" type="submit" style="margin-right:20px;margin-top:20px;">保存</button>
        <div class="clearfix"></div>
    </form> `
    </div>
</div>
{%/block%}