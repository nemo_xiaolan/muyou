{%extends "_layouts/empty.tpl"%}

{%block main_style%}
    <link rel="stylesheet" type="text/css" href="_/public/css/startpage.css" />
{%/block%}

{%block body%}
<div class="clearfix" id="startpage">
    <div id="switcher">
        {%if $C.action.name == "login"%}
            <a id="go-register" href="{%'account/register'|u%}">注册</a>
        {%else%}
            <a id="go-login" href="{%'account/login'|u%}">登陆</a>
        {%/if%}
    </div>
    <div id="startpage-wrap" style="margin-left: 0px;">
        <h1 id="logo" style="padding-top: 74px; left: -101px;">木有啦轻博客 - 博客从此简单</h1>
        {%block login_form%}{%/block%}
    </div>
    
    <div class="clearfix start-footer">
        <div class="fn-left">
            <h2><a href="{%'page/about'|u%}">关于{%$C["base"]["site_name"]%}</a></h2>
            <h2><a href="{%'explore'|u%}">探索{%$C["base"]["site_name"]%}</a></h2>
            <h2><a href="{%'category'|u%}">更多博客</a></h2>
        </div>
        <p class="fn-right">Copyright &copy; 2012 <a href="http://muyou.la">muyou.la</a></p>
    </div>
</div>
{%/block%}
