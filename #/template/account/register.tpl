{%extends "account/layout.tpl"%}


{%block login_form%}

<div id="register-wrap">
    <form autocomplete="off" method="POST" action="{%'account/register'|u%}" class="clearfix" id="reg-form">
        <div id="input-reg-email" class="input-wrap">
            <span class="input-icon"></span>
            <label>邮箱</label>
            <input type="text" value="" name="email" class="input-text">
            <div {%if !$errors.email%}style="display:none;"{%/if%} class="tip">{%$errors.email|default:"邮箱格式不正确"%}</div>
        </div>
        <div id="input-reg-pwd" class="input-wrap">
            <span class="input-icon"></span>
            <label>密码</label>
            <input type="password" name="password" class="input-text">
            <div {%if !$errors.pwd%}style="display:none;"{%/if%} class="tip">{%$errors.pwd|default:"密码最少要六位"%}</div>
        </div>
        <div id="input-reg-blogurl" class="input-wrap">
            <span class="input-icon"></span>
            <label>个性域名</label>
            <input type="text" name="sub_domain" class="input-text">
            <div {%if !$errors.sub_domain%}style="display:none;"{%/if%} class="tip">{%$errors.sub_domain|default:"个性域名不能小于5个字符"%}</div>
        </div>
        <input type="submit" value="注册" class="input-button" id="reg-submit">
    </form>
</div>

{%/block%}

{%block extra_jquery%}
    $("input:first").focus();
    $(".input-text").keydown(function(){
        $(this).prev().hide();
    }).focus(function(){
        $(this).next().hide();
    }).blur(function(){
        if(!this.value) {
            $(this).prev().show();
        }
    });
    
    var error = true;
    
    $("input[name='email']").blur(function(){
        if(this.value && !utils.is_email(this.value)) {
            $(this).next().show();
        } else {
            error = false;
            $(this).next().hide();
        }
    });
    
    $("input[name='password']").blur(function(){
        error = true;
        if(this.value && this.value.length < 6) {
            $(this).next().show();
        } else {
            error = false;
            $(this).next().hide();
        }
    });
    
    $("input[name='sub_domain']").blur(function(){
        error = true;
        var ptn = /[a-z0-9\-]{5,15}/;
        if(this.value && this.value.length < 5) {
            $(this).next().show();
        } else if(this.value && !ptn.test(this.value)) {
            $(this).next().text('只能使用5~15个位小写字母，数字和 - ').show();
        } else {
            error = false;
            $(this).next().hide();
        }
    });
    
    $("#reg-form").submit(function(){
        if(error || !$('input[name="email"]').val() || !$('input[name="password"]').val()) {
            return false;
        } else {
            return true;
        }
    });
{%/block%}