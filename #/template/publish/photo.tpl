{%extends "publish/layout.tpl"%}

{%block publisher_title%}发布照片{%/block%}
{%block publisher_subject%}
<div id="pb_photo_upload">
    <input type="file" id="uploadify" />
    <span id="spanButtonPlaceholder"></span>
    <div id="divFileProgressContainer"></div>
    <div id="pb_photo_thumbs"></div>
</div>
{%/block%}
{%block publisher_editor_name%}描述{%/block%}

{%block extra_style append%}
    <link href="_/vendor/jquery/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
{%/block%}

{%block extra_js_quote append%}
<script type="text/javascript" src="_/vendor/jquery/uploadify/jquery.uploadify.js"></script>
{%/block%}

{%block extra_jquery append%}
    var thumb_box = $("#thumbnails");
    var boxes = [];
    $('#uploadify').uploadify({
        swf  : '_/vendor/jquery/uploadify/uploadify.swf',
        uploader : BASE_URL+'{%'publish/ajax_photo'|u%}?UPLOAD_HASH={%$session_id%}',
        buttonClass: "ui-button-placeholder ui-button-blue",
        buttonText: '选择图片',
        fileTypeDesc    : '图片文件',
        fileTypeExts    : '*.jpg;*.jpeg;*.gif;*.png',
        multi : true,
        auto: true,
        checkExisting: false,
        buttonImage: '',
        removeCompleted : true,
        sizeLimit   : 409600,
        removeTimeout: 2,
        queueSizeLimit: 10,
        cancelImage: '_/vendor/jquery/uploadify/images/uploadify-cancel.png',
        onUploadSuccess : function(event, response, status){
            console.debug(response);
            var img = '_/vendor/jquery/uploadify/images/';
            response = $.parseJSON(response);
            if(response.error) {
                img = img+'error.gif';
            } else {
                img = response.thumb;
            }
            
            $('#pb_photo_thumbs').append($('<img />').attr('src', img));
        },

        onQueueComplete: function (status) {
            var the_boxes = $(boxes);
            //thumb_box.append(the_boxes).masonry('appended', the_boxes );
            //$(".the_img input[type='radio']:first").attr("checked", "checked");
            boxes = [];

            $("#submit_photos").show().click(function(){
                thumb_box.submit();
            });
        }
    });
{%/block%}

{%block before_body_end%}
<script type="text/javascript">
function save_photos() {}
</script>
{%/block%}