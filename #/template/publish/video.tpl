{%extends "publish/layout.tpl"%}

{%block publisher_title%}发布影像{%/block%}
{%block publisher_subject append%}
    <h3>视频地址</h3>
    <input type="text" name="video_url" id="video_url" placeholder="请输入视频地址，目前支持优酷，土豆等" class="ui-input-text" />
    <div id="video_parse_result" class="fn-hide"></div>
    <input type="hidden" name="flvurl" />
    <input type="hidden" name="thumbnail" />
{%/block%}

{%block publisher_editor_name%}描述{%/block%}
{%block extra_jquery append%}
    UI.placeholder();
    
    $("#video_url").change(function(){
        var val = $.trim(this.value);
        if(!val || val == $(this).attr('placeholder')) {
            return;
        }
        
        var url = "{%'publish/ajax_video'|u%}";
        $.getJSON(url, {'v': val}, function(data){
            if(!data) {
                /**
                    @todo
                */
            } else {
                utils.show_flash('#video_parse_result', data.flv);
                $('input[name="flvurl"]').val(data.flv);
                $('input[name="thumbnail"]').val(data.imgurl);
            }
        });
        
    });
{%/block%}