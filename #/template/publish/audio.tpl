{%extends "publish/layout.tpl"%}

{%block publisher_title%}发布声音{%/block%}
{%block publisher_subject%}
<div id="pb_audio_container">
    <input type="text" class="ui-input-text" placeholder="输入歌曲名、专辑或者演唱者" name="searchBox" id="pb_audio_search_box">
    <div class="pb-post-media-preview clearfix fn-hide" id="pb-audio-preview-holder">
        <a class="pb-post-media-preview-close" onclick="resetAudio()" id="pb-audio-repick-btn">重新选择音乐</a>
        <img id="pb-audio-thumb" src="">
        <div id="pb-audio-player">
            <div class="pb-music-preview-flash"></div>
            <p></p>
        </div>
        <input type="hidden" name="thumbnail" />
        <input type="hidden" name="subject" />
        <input type="hidden" name="songid" />
        <div class="clear"></div>
    </div>
    <div class="clearfix"></div>
    <div id="PopElementContainer" class="fn-hide">
        <div class="dd-suggestion pb-music-search-result">
            <ul>
            </ul>
            <div>
                <div id="pb-music-result-footer" class="clearfix pb-music-result-footer">
                    <div class="pb-music-result-count">
                    </div>
                    <div class="pb-music-result-pager clearfix">
                        <button class="fn-right ui-button-placeholder ui-button-white fn-hide pager-next">下一页</button>
                        <button class="fn-right ui-button-placeholder ui-button-white fn-hide pager-prev">上一页</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{%/block%}

{%block publisher_editor_name%}描述{%/block%}

{%block extra_js_quote append%}
<script type="text/javascript" src="_/public/js/audio.js"></script>
{%/block%}

{%block extra_jquery append%}
    var xm = new xiami();
    UI.placeholder();
{%/block%}