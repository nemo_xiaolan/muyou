{%extends "_layouts/base.tpl"%}

{%block "main-wrap"%}
    <div class="pb_main_top"><div class="l"></div><div class="r"></div></div>
    <div class="pb_wrapper">
        <h2 id="pb_main_title">{%block publisher_title%}{%/block%}</h2>
        <div class="pb_return_home"><a href="{%"home"|u%}">返回首页</a></div>
        <form method="POST" action="" id="pb_form">
            <div id="pb_main">
                <div id="pb_post_area">
                    {%block publisher_subject%}
                        <h3>标题<span>(可不填)</span></h3>
                        <input type="text" name="subject" class="ui-input-text" />
                    {%/block%}
                    <h3>{%block publisher_editor_name%}内容{%/block%}</h3>
                    <div id="pb_editor" style="width:600px;height:290px;"></div>
                    <div class="clearfix" id="pb_actions">
                        <a href="javascript:;" class="ui-button-placeholder fn-left">取消</a>
                        <a href="javascript:;" onclick="editor.sync();document.getElementById('pb_form').submit()" 
                           class="fn-right ui-button-placeholder ui-button-blue">发布</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="pb_aside">
                <div style="" class="aside-item" id="post-to-aside-holder">
                    <div class="pb-mod">
                        <label for="post-to-select">发布至</label>
                        <input type="hidden" name="post_to" id="id_post_to" value="" />
                        <div id="post-to-select" class="ui-dropdown-placeholder">
                            {%foreach from=$user->blogs item=ublog%}
                            <div class="pb_menu_text ui-dropdown-option" domain="{%$ublog.sub_domain%}" value="{%$ublog.id%}">
                                <img class="avatar" height="30" src="{%$ublog.avatar|avatar%}" />
                                <span class="pb_blog_name">{%$ublog.name%}</span>
                            </div>
                            {%/foreach%}
                        </div>
                    </div>
                </div>
                        
                <div class="pb-mod">
                    <label for="post-to-select">标签(写一个回车一下)</label>
                    <div class="aside-item" id="post-tag-holder">
                        <div id="post-tag">
                            <ul class="clearfix" id="post-tag-list">
                            </ul>
                            <div id="post-tag-input-holder">
                            <input type="text" id="post-tag-input" name="post-tag-input" class="">
                            </div>
                        </div>
                    </div>
                    <div style="display:none;" class="aside-item" id="recommand-tag-holder">
                        <ul class="clearfix" id="recommand-tag-list"></ul>
                    </div>
                </div>

                <div class="pb-mod aside-item pb-side-opt">
                    <label class="first" id="pb-set-private-holder">
                        <input type="checkbox" name="private" id="pb-set-private"> 仅自己可见
                    </label>
                    <div id="top-post-holder">
                        <label><input type="checkbox" name="toped" id="pb-top-post"> 文章置顶</label>
                        <p style="visibility: hidden;" id="top-post-tip">每个博客只能置顶一篇文章</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="pb_main_bottom"><div class="l"></div><div class="r"></div></div>
{%/block%}

{%block extra_style append%}
    <link rel="stylesheet" type="text/css" href="_/vendor/ueditor/themes/default/ueditor.css"/>
{%/block%}

{%block extra_js_quote append%}
    <script type="text/javascript" charset="utf-8" src="_/vendor/ueditor/editor_config.js"></script>
    <script type="text/javascript" charset="utf-8" src="_/vendor/ueditor/ueditor-min.js"></script>
    <script type="text/javascript">
        var editor = new baidu.editor.ui.Editor();
        editor.render('pb_editor');
    </script>
{%/block%}


{%block extra_jquery append%}
    UI.dropdown('.ui-dropdown-placeholder',{%if $single_domain%}"[domain='{%$single_domain%}']"{%else%}false{%/if%}, function(val, domain){
        $("#id_post_to").val(val);
        if(domain) {
            $("#nav li").removeClass("active");
            $("#nav-blog-list li[domain='"+domain+"']").addClass("active").siblings().removeClass("active");
        }
    });
    var keycodes = [13, 44];
    var the_tag;
    $('#post-tag').click(function(){
        $('#post-tag-input').focus();
    });
    $('#post-tag-input').keydown(function(e){
        if(e.which == 13 || e.keyCode == 13) {
            the_tag = $.trim($('#post-tag-input').val());
            if((the_tag.length < 2 && parseInt(the_tag) > 0) || !the_tag) {
                return false;
            }
            $('#post-tag-list').append('<li><span>'+the_tag+'</span><a class="delete-tag-btn" href="javascript:;" onclick="$(this).parent().next().remove();$(this).parent().remove();" title="删除">x</a></li>');
            $('#post-tag-list').append('<input type="hidden" name="post_tags[]" value="'+the_tag+'" />');
            $("#post-tag-input").val('');
            return false;
        }
    });
    {%$smarty.block.child%}
{%/block%}