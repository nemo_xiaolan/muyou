{%extends "_layouts/base.tpl"%}

{%block main_style append%}
    <link rel="stylesheet" type="text/css" href="_/public/css/following.css" />
{%/block%}

{%block "main-wrap"%}
    <div id="main" class="fn-left">
        <div id="blogs-list">
            {%assign var="the_blog_posts" value=$paginator->items%}
            <div class="clearfix" id="following-header">
                <h2 class="following-title"><span class="title-icon"></span>我关注的博客</h2>
                <ul class="clearfix" id="following-nav">
                    <li><a href="/invite">邀请好友</a></li>
                    <li><a href="/blacklist">黑名单</a></li>
                </ul>
            </div>
            <div id="invit-main">
                <div id="friend-list-holder">
                    <div class="clearfix" id="blog-search">
                        <div id="friend-search-holder">
                            <form method="get" action="/search" id="search-blog-form">
                                <div class="blog-search-tip" id="blog-search-input">
                                    <input type="text" value="通过博客名搜索" name="keyword" cloud="" class="ui-input-text" autocomplete="off" placeholder="" id="ctrltext_innerui_0" data-control="_innerui_0" style="width: 461px;">
                                </div>
                                <div id="search-blog-button">
                                    <a href="javascript:;" class="ui-button-placeholder fn-right ui-button-silver">搜索</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="friend-list">
                        <div id="search-list-content" class="friend-list-i">
                            {%include "blog/block_blogs_list.tpl"%}
                        </div>
                    </div>
                </div> 
                <div class="clearfix" id="pager-bottom"></div>
            </div>
            <div class="blog-post-list-pager">{%include "_block/paginator.tpl"%}</div>
        </div>
    </div>
    
    
    <div id="aside" class="fn-right">
        {%block aside_all%}
            {%block the_before_aside%}{%/block%}
            {%include "_block/aside_following_and_fav.tpl"%}
            {%include "_block/aside_search_form.tpl"%}
            {%block the_aside%}{%/block%}
        {%/block%}
    </div>
    <div class="fn-clear"></div>
{%/block%}