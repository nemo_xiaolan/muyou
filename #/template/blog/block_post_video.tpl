
{%assign var="post_extra" value=$item.extra|unserialize%}
{%if $post_extra.source%}
<div class="flv_source">
    <a href="{%$post_extra.source%}" target="_blank">{%$post_extra.source%}</a>
</div>
{%/if%}
{%if $item.thumbnail%}
    <div class="video-img-holder fn-left">
        <img src="{%$item.thumbnail%}" class="video_thumbnail" />
        <a href="javascript:;" class="video-play" onclick="utils.toggle_flv($(this).parent(), '{%$post_extra.flvurl%}');">播放</a>
    </div>
{%/if%}