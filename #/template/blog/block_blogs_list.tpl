<ul class="clearfix" id="f-list">
    {%foreach from=$the_blogs_list item=item name=the_blogs_list%}
        {%assign var="domain" value=$item.sub_domain%}
        <li class="clearfix{%if $smarty.foreach.the_blogs_list.last%} last-item{%/if%}">
            <a style="background-image:url({%$item.avatar|avatar:64%});" class="f-avatar" href="http://www.fashiondes.com/">FashionDes时尚要闻</a>
            <span class="user-info">
            <div class="blog-name">
            <a class="f-name" href="http://www.fashiondes.com/">{%$item.name%}</a>
            </div>
            <div class="update-time">{%$item.description%}</div>
            </span>
            <span class="action">
            <div class="do-follow b-button icon-add">
            <div cloud="" class="ui-button skin-button-willblue-lite" id="ctrlbutton_innerui_2" data-control="_innerui_2"><span class="ui-button-bg-left skin-button-willblue-lite-bg-left"></span><div class="ui-button-label skin-button-willblue-lite-label" id="ctrlbutton_innerui_2label"><span class="ui-button-text skin-button-willblue-lite-text" id="ctrlbutton_innerui_2text">关注</span></div></div>
            </div>
            <a class="cancel-follow" href="{%"blog/unfollow/$domain"|u%}">取消关注</a>
            </span>
        </li>
    {%foreachelse%}
        <li style="text-align:center;" class="clearfix last-item">
            <p style="margin-top:25px;">还没有关注博客呢~
            <a href="{%'category'|u%}">去找找喜欢的博客吧</a>
            </p>
        </li>
    {%/foreach%}
</ul>