<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Content-Language" content="zh-CN"/>
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <meta name="robots" content="index, follow" />
        <meta name="Description" content="木有啦是一个简单的轻博客，让你能简单快速地发布文字、图片、视频等各种格式内容，通过不同的风格展示来表现你的兴趣或性格。" />
        <meta name="Keywords" content="木有,木有啦,木有博客,木有轻博客" />
        <title>{%$C["base"]["site_name"]%}</title>
        <base href="{%$C['base']['root']%}" />
        <link href="_/public/css/customize.css" />
        <style>
            body{
                margin:0;padding:0;
                overflow:hidden;
                background:#444950;
                font-size:12px;
            }
            .page_preview{
                position:absolute;
                right:0;top:0;
                left:300px;
            }
            iframe{
                border:none;
                border-left:2px solid #333;
            }
            .page_left{
                background:#444950;
                overflow-x: hidden;
                overflow-y: scroll;
                color:#fff;
                padding:20px 10px;
            }
        </style>
        <script type="text/javascript" src="_/vendor/jquery/jquery.min.js?ver=1.7.1"></script>
        <script type="text/javascript" src="_/public/js/utils.js"></script>
        <script type="text/javascript" src="_/public/js/ui.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".page_preview").height($(window).height());
                $(window).resize(function(){
                    $(".page_preview").height($(window).height());
                });
            });
        </script>
    </head>
    <body>
        {%assign var="the_domain" value=$the_blog.sub_domain%}
        <div class="page_left">
            博客设置功能is comming!~
        </div>
        <div class="page_preview">
            <iframe src="{%$the_domain|blog_url%}" id="preview_frame" width="100%" height="100%"></iframe>
        </div>
        
    </body>
</html>