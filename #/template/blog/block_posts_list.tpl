{%foreach from=$the_blog_posts item=item%}
<div class="blog-post-item clearfix publisher" id="blog-post-{%$item.id%}">
    <div class="pb-avatar">
        <a style="background-image:url({%$item.avatar|avatar:64%})" href="{%$item.sub_domain|blog_url%}" class="blog-avatar" title="{%$item.blog_name%}">{%$item.blog_name%}</a>
    </div>
    <div class="pb-action-holder">
        <div class="pb-triangle"></div>
        <div class="pb-action-shadow-top"></div>
        <div class="pb-action blog-post-item-main">
            <h2><a href="{%$item.sub_domain|blog_url%}">{%$item.blog_name%}</a></h2>
            <div class="content">
                {%if $item.subject%}<h3><a href="{%$item.sub_domain|blog_url%}/posts/{%$item.id%}">{%$item.subject%}</a></h3>{%/if%}
                {%if $item.type == "audio"%}
                    {%include 'blog/block_post_audio.tpl'%}
                {%/if%}
                {%if $item.type == "video"%}
                    {%include 'blog/block_post_video.tpl'%}
                {%/if%}
                {%$item.content%}
                <div class="clearfix"></div>
            </div>
            <div class="post-item-tags">
                {%foreach from=$item.the_tags item=tag%}
                    {%assign var="the_tag_name" value=$tag.name|urlencode%}
                    <a href="{%"search/all_tags/$the_tag_name"|u%}">#{%$tag.name%}</a>
                {%/foreach%}
            </div>
            <div class="post-item-acts" postid="{%$item.id%}">
                {%assign var="postid" value=$item.id%}
                {%if $user->uid == $item.uid or $item.blog_manager ==$user->uid%}
                    <a class="feed-del" href="{%"posts/delete/$postid"|u%}">删除</a>
                    <a href="" class="feed-edit">编辑</a>
                {%else%}
                    <a class="feed-fav{%if $item.is_fav%} faved{%/if%}" onclick="utils.toggle_fav($(this), {%$item.id%});" title="喜欢">喜欢</a>
                    <a href="">转载</a>
                {%/if%}
                <a class="feed-cmt">回应{%if $item.replies%}({%$item.replies%}){%/if%}</a>
                <a class="feed-nt">热度{%if $item.favorites%}({%$item.favorites%}){%/if%}</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="pb-action-shadow-bottom"></div>
        
        <div class="post-item-extra">
            {%* @todo 回应/热度的收起展开*%}
        </div>
    </div>
</div>
{%foreachelse%}
    <div style="text-align:center;margin-left:85px;">没有找到相关文章</div>
{%/foreach%}