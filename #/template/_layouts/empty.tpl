<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="Content-Language" content="zh-CN"/>
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <meta name="robots" content="index, follow" />
        <meta name="Description" content="木有啦是一个简单的轻博客，让你能简单快速地发布文字、图片、视频等各种格式内容，通过不同的风格展示来表现你的兴趣或性格。" />
        <meta name="Keywords" content="木有,木有啦,木有博客,木有轻博客" />
        <title>{%if $page_title%}{%$page_title%} - {%/if%}{%$C["base"]["site_name"]%}</title>
        <base href="{%$C['base']['root']%}" />
        <link rel="stylesheet" type="text/css" href="_/public/css/base.css" />
        {%block main_style%}
        <link rel="stylesheet" type="text/css" href="_/public/css/style.css" />
        {%/block%}
        {%block extra_style%}{%$smarty.block.child%}{%/block%}
        <script type="text/javascript">
            var BASE_URL = '{%$C['base']['root']%}';
        </script>
    </head>
    <body>
        {%block body%}
        
        {%/block%}
        <script type="text/javascript" src="_/vendor/jquery/jquery.min.js?ver=1.7.1"></script>
        <script type="text/javascript" src="_/public/js/utils.js"></script>
        <script type="text/javascript" src="_/public/js/ui.js"></script>
        {%block extra_js_quote%}{%/block%}
        <script type="text/javascript">
            $(document).ready(function(){
                {%block extra_jquery%}{%/block%}
                UI.button('.ui-button-placeholder');
            });
        </script>
        {%block before_body_end%}{%/block%}
    </body>
</html>