{%extends "_layouts/empty.tpl"%}

{%block "body"%}
    <div id="header">
        <div class="the_inner">
            <div id="logo-holder">
                <h1 id="logo"><a href="{%$C['base']['root']%}">{%$C["base"]["site_name"]%}</a></h1>
                <a style="display:none;" class="header-pop-notice" id="new-feed-count"></a>
            </div>
            {%if $user->is_login%}
                <ul id="nav">
                    <li id="nav-index" {%if !$single_domain%}class="active"{%/if%}>
                        <a href="{%$C['base']['root']%}" class="nav-item">首页</a>
                    </li>
                </ul>
                <div class="misc-action" id="J_HeaderMiscAction">
                    <ul class="action-list">
                        <li class="action-item"><a title="发现" href="{%'explore'|u%}" class="action-link discovery">发现</a></li>
                        <li class="action-item"><a title="私信" href="{%'message/inbox'|u%}" class="action-link inbox">私信</a></li>
                        <li class="action-item"><a title="模板公园" href="{%'themes'|u%}" class="action-link themegarden">模板公园</a></li>
                        <li class="action-item"><a title="设置" href="{%'settings'|u%}" class="action-link settings">设置</a></li>
                        <li class="action-item"><a title="退出" href="{%'account/logout'|u%}" class="action-link logout">退出</a></li>
                    </ul>
                </div>
                <ul id="nav-blog-list">
                    {%foreach from=$user->blogs item=ublog%}
                        {%assign var="domain" value=$ublog.sub_domain%}
                        <li domain="{%$ublog.sub_domain%}" class="blog-item{%if $single_domain==$domain%} active{%/if%}" id="nav-blog-{%$ublog.id%}">
                            <a href="{%"blog/$domain"|u%}">
                                <span class="nav-blog-name">{%$ublog.name%}</span>
                            </a>
                        </li>
                    {%/foreach%}
                </ul>
                <div id="nav-blog-action">
                    <a class="nav-more-blog" id="nav-more-blog" style="display: none;">更多</a>
                    <a title="再创建一个博客" href="{%'blog/new'|u%}" class="nav-new-blog" id="nav-new-blog">再创建一个博客</a>
                    <span style="display:none;" class="header-pop-notice large-pop-notice blog-pop-notice" id="nav-more-blog-notice">New</span>
                </div>
            {%else%}
                <div id="no-login-search-holder" class="no-login-search">
                    <form id="no-login-search-form" action="{%'search'|u%}" method="POST">
                        <input type="text" name="search_keyword" class="input-tip"  id="no-login-search-input" />
                        <input type="hidden" name="search_target" value="all_tags" />
                        <button type="submit" id="no-login-go-search">搜索</button>
                    </form>
                </div>
                <ul class="no-login" id="sub-nav">
                    <li><a href="{%'account/login'|u%}">登录</a></li>
                    <li><a href="{%'account/register'|u%}">注册</a></li>
                    <li><a href="{%'category'%}">推荐</a></li>
                    <li><a href="{%'explore'|u%}">发现</a></li>
                    <li><a href="{%'page/about'|u%}">关于</a></li>
                </ul>
            {%/if%}
        </div>
    </div>
    <div id="main-wrap" class="the_inner">
        {%block "main-wrap"%}{%/block%}
    </div>
{%/block%}