{%extends "_layouts/base.tpl"%}

{%block extra_style%}
<link rel="stylesheet" type="text/css" href="_/public/css/category.css" />
{%/block%}

{%block "main-wrap"%}
<div class="recommend-wrapper clearfix">
    <div class="taglist-holder">
        <ul class="taglist clearfix">
            {%foreach from=$the_categories item=tc%}
            <li{%if $current_id==$tc.id%} class="selected"{%/if%}>
                {%assign var="tc_id" value=$tc.id%}
                <a href="{%"category/$tc_id"|u%}">{%$tc.name%}</a>
            </li>
            {%/foreach%}
            <li{%if $current_id==0%} class="selected"{%/if%}>
                <a href="{%"category/0"|u%}">其他</a>
            </li>
        </ul>
    </div>
        
    <div id="blogList" class="bloglist clearfix">
        {%foreach from=$blogs_list item=bl%}
        <div data-followed="0" class="blogitem">
            <a target="_blank" href="{%$bl.sub_domain|blog_url%}" style="background-image:url({%$bl.avatar|avatar%});" class="blogitem-avatar ">韦小良</a>
            <h5 class="blogitem-title">
                <a target="_blank" href="{%$bl.sub_domain|blog_url%}">{%$bl.name%}</a>
            </h5>
            <div class="blogitem-desc">{%$bl.description%}</div>
            {%assign var="the_domain" value=$bl.sub_domain%}
            {%if $bl.is_following%}
                <a href="{%"blog/unfollow/$the_domain"|u%}" class="ui-button-placeholder ui-button-blue-lite fn-left blogitem-follow">取消关注</a>
            {%else%}
                <a href="{%"blog/follow/$the_domain"|u%}" class="ui-button-placeholder ui-button-blue-lite fn-left blogitem-follow">关注</a>
            {%/if%}
            {%if $bl.dateline+(3600*24*7) > $smarty.const.CURRENT_TIMESTAMP%}
            <span class="blogitem-new">NEW</span>
            {%/if%}
        </div>
        {%/foreach%}
    </div>
        
</div>
{%/block%}