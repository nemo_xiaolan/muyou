<?php

/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

return array(
    "base" => array(
        "root" => "http://127.0.0.1/muyou/",
        #"root" => "http://muyou.com/",
        #"root" => "http://a.muyou.la/",
        #"host" => "muyou.com",
        "url_type" => "pathinfo",
        "site_name" => "木有轻博客",
        "runmode" => "devel"
    ),
    "database" => array(
        "dsn" => "mysql:host=127.0.0.1;dbname=vb",
        "usr" => "root", "pwd" => "root", "pre" => "my_"
    ),
    "tpl" => array(
        'left_delimiter'  => '{%', 
        'right_delimiter' => '%}',
        //'template_dir'    => PROJECT_DIR.DS.'template',
        'template_dir'    => array(
            PROJECT_DIR.DS.'template',
        ),
        'compile_dir'     => PROJECT_DIR.DS.'tmp'.DS.'tpl_compiled',
        'caching'         => false,
        'cache_dir'       => PROJECT_DIR.DS.'tmp'.DS.'tpl_cache',
        'cache_lifetime'  => 600,
        'compile_check'   => true,
    ),
    "denied_domain_list" => array("admin", "dm", "www", "login", "account")
);