{%extends "template/layout.tpl"%}

{%block page_main%}
    {%foreach from=$the_posts item=item%}
        {%if !$item.private or ($user->uid == $item.uid or $item.blog_manager == $user->uid)%}
            <div class="blog-post-item">
                {%if $item.subject%}
                    <h2><a href="{%$the_blog.sub_domain|blog_url%}/posts/{%$item.id%}">{%$item.subject%}</a></h2>
                {%/if%}
                {%if $item.type == "audio"%}
                    {%include 'blog/block_post_audio.tpl'%}
                {%/if%}
                {%$item.content%}

                <div class="post-item-acts">
                    {%if $user->uid == $item.uid or $item.blog_manager ==$user->uid%}
                        <a class="feed-del">删除</a>
                        <a href="" class="feed-edit">编辑</a>
                    {%else%}
                        <a class="feed-fav " title="喜欢">喜欢</a>
                        <a href="">转载</a>
                    {%/if%}
                    <a class="feed-cmt">回应{%if $item.replies%}({%$item.replies%}){%/if%}</a>
                    <a data-type="text"class="feed-nt">热度{%if $item.favorites%}({%$item.favorites%}){%/if%}</a>
                </div>
                <div class="clearfix"></div>
            </div>
        {%/if%}
    {%/foreach%}
    {%include "_block/paginator.tpl"%}
{%/block%}