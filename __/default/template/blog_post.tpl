{%extends "template/layout.tpl"%}

{%block page_main%}
<div class="blog-post-item">
    {%if $the_post.subject%}
        <h2><a href="{%$the_blog.sub_domain|blog_url%}/posts/{%$the_post.id%}">{%$the_post.subject%}</a></h2>
    {%/if%}
    {%if $the_post.type == "audio"%}
        {%assign var="item" value=$the_post%}
        {%assign var="post_extra" value=$the_post.extra|unserialize%}
        {%include 'blog/block_post_audio.tpl'%}
    {%/if%}
    {%$the_post.content%}

    <div class="post-item-acts">
        {%if $user->uid == $the_post.uid or $the_post.blog_manager ==$user->uid%}
            <a class="feed-del">删除</a>
            <a href="" class="feed-edit">编辑</a>
        {%else%}
            <a class="feed-fav{%if $the_post.is_fav%} faved{%/if%}" onclick="utils.toggle_fav($(this), {%$the_post.id%});" title="喜欢">喜欢</a>
            <a href="">转载</a>
        {%/if%}
        <a class="feed-cmt">回应{%if $the_post.replies%}({%$the_post.replies%}){%/if%}</a>
        <a class="feed-nt">热度{%if $the_post.favorites%}({%$the_post.favorites%}){%/if%}</a>
    </div>
    <div class="clearfix"></div>
</div>
{%/block%}