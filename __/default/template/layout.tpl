{%extends "_blog_layouts/base.tpl"%}
{%block main_style%}
    <link rel="stylesheet" type="text/css" href="__/default/statics/style.css" />
{%/block%}

{%block body%}
<div id="page-left">
    {%block aside%}
    <h1><a href="{%$the_blog.sub_domain|blog_url%}">{%$the_blog.name%}</a></h1>
    {%/block%}
    <ul class="blog-actions-nav">
        <li><a href="">投稿</a></li>
        <li><a href="">私信</a></li>
        <li><a href="">存档</a></li>
        <li><a href="">订阅</a></li>
    </ul>
</div>

<div id="page-main-wrap">
    {%block page_main%}
        
    {%/block%}
</div>
{%/block%}