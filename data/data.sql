-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 03 月 26 日 11:47
-- 服务器版本: 5.0.45
-- PHP 版本: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `vb`
--

-- --------------------------------------------------------

--
-- 表的结构 `my_blogs`
--

DROP TABLE IF EXISTS `my_blogs`;
CREATE TABLE IF NOT EXISTS `my_blogs` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL,
  `catid` smallint(5) NOT NULL default '0',
  `avatar` varchar(255) default NULL,
  `name` varchar(50) default NULL,
  `sub_domain` varchar(50) NOT NULL,
  `theme` varchar(50) NOT NULL default 'default',
  `description` varchar(255) default NULL,
  `dateline` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `sub_domain` (`sub_domain`),
  KEY `name` (`name`),
  KEY `uid` (`uid`),
  KEY `theme` (`theme`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `my_blogs`
--

INSERT INTO `my_blogs` (`id`, `uid`, `catid`, `avatar`, `name`, `sub_domain`, `theme`, `description`, `dateline`) VALUES
(1, 1, 1, '', 'muyou', 'muyou', 'default', NULL, 0),
(2, 2, 1, NULL, 'xiaolan', 'xiaolan', 'default', NULL, 0),
(3, 1, 1, NULL, 'help', 'muyou-help', 'default', NULL, 0);

-- --------------------------------------------------------

--
-- 表的结构 `my_blog_categories`
--

DROP TABLE IF EXISTS `my_blog_categories`;
CREATE TABLE IF NOT EXISTS `my_blog_categories` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `listorder` smallint(3) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`,`listorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `my_blog_categories`
--

INSERT INTO `my_blog_categories` (`id`, `name`, `listorder`) VALUES
(1, '时尚', 0),
(2, '艺术', 0),
(3, '文化', 0),
(4, '摄影', 0),
(5, '设计', 0),
(6, 'IT', 0),
(7, '旅行', 0),
(8, '插画', 0),
(9, '美食', 0);

-- --------------------------------------------------------

--
-- 表的结构 `my_blog_posts`
--

DROP TABLE IF EXISTS `my_blog_posts`;
CREATE TABLE IF NOT EXISTS `my_blog_posts` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL,
  `blogid` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `subject` varchar(255) default NULL,
  `content` text NOT NULL,
  `thumbnail` varchar(255) default NULL,
  `dateline` int(11) NOT NULL,
  `favorites` smallint(11) NOT NULL default '0',
  `replies` smallint(11) NOT NULL default '0',
  `private` smallint(1) NOT NULL default '0',
  `toped` smallint(1) NOT NULL default '0',
  `is_submit` smallint(1) NOT NULL default '0',
  `extra` text,
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`blogid`,`type`,`dateline`,`favorites`,`replies`),
  KEY `is_submit` (`is_submit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `my_blog_post_photos`
--

DROP TABLE IF EXISTS `my_blog_post_photos`;
CREATE TABLE IF NOT EXISTS `my_blog_post_photos` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `blogid` int(11) NOT NULL,
  `save_path` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`postid`,`blogid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `my_favorites`
--

DROP TABLE IF EXISTS `my_favorites`;
CREATE TABLE IF NOT EXISTS `my_favorites` (
  `uid` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  KEY `uid` (`uid`,`postid`,`dateline`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `my_favorites`
--

INSERT INTO `my_favorites` (`uid`, `postid`, `dateline`) VALUES
(1, 9, 1332143134),
(2, 3, 1332052549),
(2, 4, 1332052547),
(2, 7, 1332052546);

-- --------------------------------------------------------

--
-- 表的结构 `my_follow`
--

DROP TABLE IF EXISTS `my_follow`;
CREATE TABLE IF NOT EXISTS `my_follow` (
  `follower` int(11) NOT NULL,
  `followed` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  KEY `follower` (`follower`,`followed`,`dateline`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `my_follow`
--

INSERT INTO `my_follow` (`follower`, `followed`, `dateline`) VALUES
(1, 1, 1332555008),
(1, 2, 1332554354),
(1, 3, 1332555009),
(2, 1, 1332039629);

-- --------------------------------------------------------

--
-- 表的结构 `my_session`
--

DROP TABLE IF EXISTS `my_session`;
CREATE TABLE IF NOT EXISTS `my_session` (
  `sesskey` char(32) NOT NULL,
  `expiry` int(11) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`sesskey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `my_session`
--

INSERT INTO `my_session` (`sesskey`, `expiry`, `value`) VALUES
('gims2ieq18kacjk769oqthu6m7', 1332753203, 'member|a:4:{s:3:"uid";s:1:"1";s:5:"email";s:22:"nemo.xiaolan@gmail.com";s:8:"is_admin";s:1:"0";s:5:"blogs";a:2:{i:0;a:9:{s:2:"id";s:1:"1";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";s:0:"";s:4:"name";s:5:"muyou";s:10:"sub_domain";s:5:"muyou";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}i:1;a:9:{s:2:"id";s:1:"3";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";N;s:4:"name";s:4:"help";s:10:"sub_domain";s:10:"muyou-help";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}}}test|i:123;'),
('2fb0dg1olcl5uctlkros5mvth2', 1332754033, ''),
('tre4fg90kf9m39pr7mv62slto2', 1332753822, ''),
('diu1ah759bo9t3h6gabgs38k77', 1332808985, 'member|a:4:{s:3:"uid";s:1:"1";s:5:"email";s:22:"nemo.xiaolan@gmail.com";s:8:"is_admin";s:1:"0";s:5:"blogs";a:2:{i:0;a:9:{s:2:"id";s:1:"1";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";s:0:"";s:4:"name";s:5:"muyou";s:10:"sub_domain";s:5:"muyou";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}i:1;a:9:{s:2:"id";s:1:"3";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";N;s:4:"name";s:4:"help";s:10:"sub_domain";s:10:"muyou-help";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}}}'),
('nkr0fv9c95t0kg59kv6j0tlal5', 1332809003, ''),
('ch54ocumtk562i8nos1mmctqn7', 1332810378, 'member|a:4:{s:3:"uid";s:1:"1";s:5:"email";s:22:"nemo.xiaolan@gmail.com";s:8:"is_admin";s:1:"0";s:5:"blogs";a:2:{i:0;a:9:{s:2:"id";s:1:"1";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";s:0:"";s:4:"name";s:5:"muyou";s:10:"sub_domain";s:5:"muyou";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}i:1;a:9:{s:2:"id";s:1:"3";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";N;s:4:"name";s:4:"help";s:10:"sub_domain";s:10:"muyou-help";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}}}'),
('parpe36ng8llm97pm2ii1pdh01', 1332810304, ''),
('r7eu2sum66250tl4fgop76qud3', 1332810329, ''),
('5eijha08ctfs6ahklg1vqcj3s6', 1332815524, 'member|a:4:{s:3:"uid";s:1:"1";s:5:"email";s:22:"nemo.xiaolan@gmail.com";s:8:"is_admin";s:1:"0";s:5:"blogs";a:2:{i:0;a:9:{s:2:"id";s:1:"1";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";s:0:"";s:4:"name";s:5:"muyou";s:10:"sub_domain";s:5:"muyou";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}i:1;a:9:{s:2:"id";s:1:"3";s:3:"uid";s:1:"1";s:5:"catid";s:1:"1";s:6:"avatar";N;s:4:"name";s:4:"help";s:10:"sub_domain";s:10:"muyou-help";s:5:"theme";s:7:"default";s:11:"description";N;s:8:"dateline";s:1:"0";}}}');

-- --------------------------------------------------------

--
-- 表的结构 `my_tags`
--

DROP TABLE IF EXISTS `my_tags`;
CREATE TABLE IF NOT EXISTS `my_tags` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `my_tag_items`
--

DROP TABLE IF EXISTS `my_tag_items`;
CREATE TABLE IF NOT EXISTS `my_tag_items` (
  `tagid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `blogid` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  KEY `tagid` (`tagid`,`itemid`,`type`),
  KEY `blogid` (`blogid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `my_users`
--

DROP TABLE IF EXISTS `my_users`;
CREATE TABLE IF NOT EXISTS `my_users` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `reg_time` int(11) NOT NULL,
  `reg_ip` int(11) NOT NULL,
  `last_login_time` int(11) NOT NULL,
  `last_login_ip` int(11) NOT NULL,
  `is_admin` smallint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `email` (`email`,`password`,`last_login_time`),
  KEY `is_admin` (`is_admin`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `my_users`
--

INSERT INTO `my_users` (`id`, `email`, `password`, `reg_time`, `reg_ip`, `last_login_time`, `last_login_ip`, `is_admin`) VALUES
(1, 'nemo.xiaolan@gmail.com', '844f47f7428ab4a738f__13__19', 1331361836, 2130706433, 1332729113, 2130706433, 0),
(2, '335454250@qq.com', '0fffea69b1844f47f7428a__3__22', 1331943718, 2130706433, 1332117422, 2130706433, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
