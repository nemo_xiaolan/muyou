<?php
/**
 * @author Nemo <nemo.xiaolan@gmail.com>
 */

error_reporting(E_ALL^E_NOTICE);

define("DS", DIRECTORY_SEPARATOR);
define("IN_APP", true);
define("CURRENT_TIMESTAMP", time());
define("ENTRY_DIR", dirname(__FILE__));
define("PROJECT_DIR", ENTRY_DIR.DS."#");


/**
 * 是否windows服务器
 * 是否IE
 */
define('IS_WIN', !strncasecmp(PHP_OS, 'win', 3));
define('IS_IE', strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') ? true : false);
define('IS_IE6', strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') ? true : false);

require PROJECT_DIR.DS."#.php";