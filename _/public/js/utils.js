var utils = {
    is_email: function(email) {
        return email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]{2,4}$/) != -1;
    },
    
    show_flash: function(container, flash, width, height) {
        width = width ? parseInt(width) : 560;
        height = height ? parseInt(height) : 420;
        
        $(container)
        .removeClass("fn-hide")
        .html('<embed src="'+flash+'" allowFullScreen="true" quality="high" width="'+width+'" height="'+height+'" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>');
    },
    //页面中显示
    toggle_flv: function(obj, flash, width, height) {
        width = width ? parseInt(width) : 505;
        height = height ? parseInt(height) : 379;
        obj
        .after('<div><embed src="'+flash+'" allowFullScreen="true" quality="high" width="'+width+'" height="'+height+'" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>')
        .remove();
    },
    toggle_fav: function(obj, postid, callback) {
        $.post(BASE_URL+"posts/ajax_favorite/", {"postid": postid}, function(data){
            if(typeof(callback) == "function") {
                callback(obj);
            } else {
                obj.toggleClass("faved");
            }
        });
    },
    toggle_by_focus: function(obj, a) {
        a.focus()
        .blur(function(){
            setTimeout(function(){obj.addClass("fn-hide")}, 100);
        });
        obj.toggleClass("fn-hide");
        
        obj.find("li").click(function(){
            $(this).addClass("on").siblings().removeClass("on");
            console.debug($(this));
            $("input[name='search_target']").val($(this).attr("data"));
        });
    }
};