var UI = {
    button: function(objs) {
        objs = $(objs);
        if(typeof(objs) == "object") {
            var obj;
            var html;
            for(i=0; i<objs.length; i++) {
                html = $('<div class="ui-button-holder"><span></span><a class="ui-button"></a></div>');
                obj  = $(objs[i]);
                obj.removeClass("ui-button-placeholder");
                html.addClass(obj.attr("class"))
                    .attr("id", obj.attr("id"));
                html.find(".ui-button")
                    .text(obj.html())
                    .attr("href", obj.attr("href"));
                if(typeof(obj.attr("onclick")) != "undefined") {
                    html.find(".ui-button")
                    .attr("onclick", obj.attr("onclick"));
                }
                    
                obj.after(html).remove();
            }
            
            $('.ui-button-holder').hover(function(){
                $(this).addClass("ui-button-holder-hover");
            }, function(){
                $(this).removeClass("ui-button-holder-hover");
            }).mousedown(function(){
                $(this).addClass("ui-button-holder-active");
            }).mouseup(function(){
                $(this).removeClass("ui-button-holder-active");
            });
        }
    },
    dropdown: function(obj, init_value, callback) {
        obj = $(obj);
        
        var the_options_html = $('<div class="ui-dropdown-options"></div>');
        var current_html = $('<div class="ui-dropdown-current"><div class="pb_menu_text ui-dropdown-current-content"></div><div class="ui-arrow-bottom"></div></div>');
        the_options_html.append(obj.html());
        
        obj.html(current_html);
        obj.append(the_options_html);
        
        var current = obj.find(".ui-dropdown-current");
        var the_options = obj.find(".ui-dropdown-options");
        
        //默认值
        var init
        if(init_value) {
            init = obj.find(".ui-dropdown-option"+init_value);
            obj.find(".ui-dropdown-current-content").html(init.html());
            if(typeof(callback) == "function"){
                callback(init.attr("value"), init.attr("domain"));
            }
        } else {
            init = obj.find(".ui-dropdown-option:first");
            obj.find(".ui-dropdown-current-content").html(init.html());
            if(typeof(callback) == "function"){
                callback(init.attr('value'), init.attr("domain"));
            }
        }
        
        
        current.click(function(){
            the_options.toggle();
            obj.hover(function(){
            }, function(){
                setTimeout(function(){ the_options.hide(); }, 300);
            });
        });
        obj.find(".ui-dropdown-option")
           .click(function(){
               obj.find(".ui-dropdown-current-content").html(this.innerHTML);
               the_options.hide();
               if(typeof(callback) == "function"){
                   callback($(this).attr('value'), $(this).attr("domain"));
               }
           })
           .hover(function(){
               $(this).addClass('ui-dropdown-option-hover');
           }, function(){
               $(this).removeClass('ui-dropdown-option-hover');
           });
    },
    placeholder: function(){
        var default_value;
        var obj;
        $('input[placeholder]').each(function(){
            obj = $(this);
            this.value = obj.attr("placeholder");
            default_value = this.value;
            $(this).focus(function(){
                if(!$.trim(obj.val()) || obj.val()== default_value) {
                    obj.val('');
                }
            }).blur(function(){
                if(!$.trim(obj.val())) {
                    obj.val(default_value);
                }
            });
        });
        $('input[placeholder]').focus(function(){
            
        }).blur(function(){
            
        });
    }
};