var xiami = function() {
    
    this.options = {
        searchBox: "#pb_audio_search_box",
        resultContainer: "#pb_audio_search_result"
    };
    
    var pager = {
        perpage: 8
    }
    
    var xm = this;
    var last_keyword = false;
    var current_page = 1;
    var limit;
    var offset=8;
    $(xm.options.searchBox).keydown(function(e){
        setTimeout(function(){
            //if(current_keyword && current_keyword != $.trim($(xm.options.searchBox).val())) {
                xm.doSearch();
            //}
        }, 1000);
    });
    
    this.doSearch = function(page, keyword) {
        current_page = page ? page : 1;
        if(!keyword) {
            keyword = $.trim($(xm.options.searchBox).val());
        }
        if(!keyword) {
            $('#PopElementContainer').addClass("fn-hide");
            return false;
        }
        
        if(keyword == last_keyword && !page) {
            return false;
        }
        
        last_keyword = keyword;
        
        last_keyword = keyword;
        var url = 'http://www.xiami.com/app/nineteen/search/key/'+encodeURIComponent(keyword)+'/page/'+current_page+'?random='+new Date().getTime()+'?callback=__parseSearchResult__'
        console.debug(url);
        $.getScript(url);
    };
    
    this.parseSearchResult = function(data) {
        if(data.results.length > 0) {
            $(".pb-music-result-count").html("共"+data.total+"首和“"+data.key+"”相关的音乐");
            $('#PopElementContainer').removeClass("fn-hide");
            var list_container = $(".dd-suggestion ul").html('');
            var song_name, artist_name, album_name;
            for(i=0; i<data.results.length;i++) {
                song_name = $.trim(decodeURIComponent(data.results[i].song_name).replace(/\+{1}/g, " "));
                song_id = parseInt(data.results[i].song_id);
                artist_name = $.trim(decodeURIComponent(data.results[i].artist_name).replace(/\+{1}/g, " "));
                album_name = $.trim(decodeURIComponent(data.results[i].album_name).replace(/\+{1}/g, " "));
                list_container.append('<li onclick="renderSong('+song_id+')"><span class="pb-music-song">'+song_name+'</span><span class="separator">-</span><span class="pb-music-artist">'+artist_name+' ['+album_name+'] </span></li>');
            }
            limit = current_page*offset;
            if(data.total > limit) {
                $('.pb-music-search-result .pager-next')
                    .removeClass("fn-hide")
                    .attr('onclick', "doSearch("+(current_page+1)+", "+last_keyword+")");
            } else {
                $('.pb-music-search-result .pager-next')
                    .addClass("fn-hide")
                    .attr("onclick", "return false;");
            }
            
            if(current_page > 1) {
                $('.pb-music-search-result .pager-prev')
                    .removeClass("fn-hide")
                    .attr('onclick', "doSearch("+(current_page-1)+", "+last_keyword+")");
            } else {
                $('.pb-music-search-result .pager-prev')
                    .addClass("fn-hide")
                    .attr("onclick", "return false");
            }
            
        } else {
            $('#PopElementContainer').removeClass("fn-hide");
            $(".pb-music-result-count").html("没有找到“"+data.key+"”相关的音乐");
        }
    };
    
    this.renderSong = function(songid) {
        //var url = 'http://www.xiami.com/widget/xml-single/uid/0/sid/'+songid;
        var url = BASE_URL+'publish/ajax_audio?songid='+songid;
        $.get(url, function(data){
            if(data){
                $("#PopElementContainer").addClass('fn-hide');
                $("#pb_audio_search_box").addClass('fn-hide');
                data = $.parseJSON(data);
                var intro = data.song_name +' - '+ data.artist_name+' ['+data.album_name+']';
                data.album_cover = data.album_cover.replace(/\_3\.jpg$/g, "_1.jpg").replace(/\_3\.jpeg$/g, "_1.jpeg")
                $("#pb-audio-thumb").attr("src", data.album_cover);
                $('#pb-audio-player p').append(intro);
                $(".pb-music-preview-flash")
                   .html('<embed height="33" width="257" wmode="transparent" type="application/x-shockwave-flash" src="http://www.xiami.com/widget/0_'+data.song_id+'/singlePlayer.swf"></embed>');
                $("#pb-audio-preview-holder").removeClass("fn-hide");
                $("input[name='thumbnail']").val(data.album_cover);
                $("input[name='subject']").val(intro);
                $("input[name='songid']").val(data.song_id);
                //$("#pb_audio_container").append();
            }
        });
    };
    
    this.resetAudio = function() {
        $('#pb-audio-preview-holder').addClass('fn-hide');
        $('#pb_audio_search_box').removeClass('fn-hide').focus();
    }
    
    window.__parseSearchResult__ = this.parseSearchResult;
    window.renderSong = this.renderSong;
    window.resetAudio = this.resetAudio;
    window.doSearch = this.doSearch;
}